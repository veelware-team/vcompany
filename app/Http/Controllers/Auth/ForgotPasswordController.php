<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return Inertia::render('Public/Auth/Password/Email');
    }
    public function validateUserEmail(Request $request){
        $message = 'success';
        $continue = true;
        $color = 'error';
        $user = User::where('email',trim($request->email))->first();
        if(!$user){
            $message = 'Lo sentimos, no se ha encontrado un usuario con ese correo electrónico.';
            $continue= false;
        }else{
            if($user->active == false){
                $message = 'Lo sentimosn, tu usuario no se encuentra activo, comunicate con nosotros.';
                $continue= false;
            }
        }
        return response()->json([
            'message' => $message,
            'continue' => $continue,
            'color' => $color,
        ]);
    }
}
