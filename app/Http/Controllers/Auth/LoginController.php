<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class LoginController extends Controller
{

    use AuthenticatesUsers;


    //protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(){
        return 'code';
    }

    public function redirectTo()
    {
        if(Auth::user()->can('private.dashboard.admin') || Auth::user()->can('private.dashboard.worker')){
            return  route('private.dashboard');
        }else{
            return  route('public.index');
        }
    }
    public function showLoginForm(){
        return Inertia::render('Public/Auth/Login');
    }

    public function login(Request $request){
        $message ='';
        $status='202';
        $color ='error';
        $user = User::where('code',trim($request->code))->first();
        if(!$user){
            $message  = 'Lo sentimos, el codigo ingresado no existe en el sistema.';
        }else{
            if($user->active == false){
                $message  = 'Lo sentimos, tu usuario no se encuentra activo, por favor comunicate con el administrador del sistema.';
            }else{
                if(Auth::attempt(['code'=> $request->code, 'password'=> $request->password,'active'=>1]))
                {
                    $request->session()->regenerate();
                    /*return redirect()->intended($this->redirectTo());*/
                    $message ='Hola '.$user->name.', nos alegra que estes de vuelta.';
                    $status='200';
                    $color ='success';
                }else{
                    $message  = 'Lo sentimos, tu contraseña es incorrecta';
                }
            }
        }
        return response()->json([
            'message' => $message,
            'color' => $color,
        ],$status);
    }

    public function goToPage(){
        return redirect()->intended($this->redirectTo());
    }

}
