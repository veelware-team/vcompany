<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class ResetPasswordController extends Controller
{

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    /*protected $redirectTo = RouteServiceProvider::HOME;*/

    public function redirectTo()
    {
        if(Auth::user()->can('private.dashboard.admin') || Auth::user()->can('private.dashboard.worker')){
            return  route('private.dashboard');
        }else{
            return  route('public.index');
        }
    }
    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');
        $user = User::where('email',$request->email)->first();
        $firstname= $user->userFirstName();
        $data = ['data_token' => $token, 'data_email' => $request->email,'data_firstname'=>$firstname];
        //return Inertia::render('Public/Auth/Password/Reset',$data);
        return Inertia::render('Public/Auth/Password/Reset',$data);
    }
}
