<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Inertia\Inertia;


class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:customers.index')->only('index');
        $this->middleware('can:customers.new')->only('store');
        $this->middleware('can:customers.update')->only('update');
        $this->middleware('can:customers.destroy')->only('destroy');
    }

    public function index()
    {
        return Inertia::render('Private/Customers/Index');
    }

    public function list()
    {
        $customers = Customer::query()->select()->where('id','<>','1')->orderBy('id','desc')->get()->map(function ($customerTemp){
            return [
                'id'=>$customerTemp->id,
                'name'=>$customerTemp->name,
                'nickname'=>$customerTemp->nickname,
                'phone'=>$customerTemp->phone,
                'email'=>$customerTemp->email,
                'nit'=>$customerTemp->nit,
                'birthdate'=>$customerTemp->birthdate,
                'birthdate_formated' => $customerTemp->birthDateFormated(),
                'adress'=>$customerTemp->adress,
            ];
        });
        return response()->json(['customers'=>$customers]);
    }

    public function listDense(){
        $customers = Customer::query()->select()->where('id','<>','1')->orderBy('id','desc')->get()->map(function ($customerTemp){
            return [
                'id'=>$customerTemp->id,
                'name'=>$customerTemp->name,
                'nickname'=>$customerTemp->nickname,
                'phone'=>$customerTemp->phone,
                'email'=>$customerTemp->email,
                'nit'=>$customerTemp->nit,
            ];
        });
        return response()->json(['customers'=>$customers]);
    }

    public function store(Request $request)
    {
        $customerInstance = Customer::create($request->only([
            'name',
            'nickname',
            'phone',
            'email',
            'nit',
            'birthdate',
            'adress',
        ]));
        return response()->json(['message'=>'El cliente se ha guardado exitosamente.']);
    }

    public function update(Request $request, String $customerId)
    {
        $customer = Customer::find($customerId);
        $customer->update($request->only([
            'name',
            'nickname',
            'phone',
            'email',
            'nit',
            'birthdate',
            'adress',
        ]));
        return response()->json(['message'=>'El cliente se ha actualizado exitosamente.']);
    }

    public function destroy(String $customerId)
    {
        $customer = Customer::find($customerId);
        $customer->delete();
        return response()->json(['message'=>'El cliente se ha eliminado exitosamente.']);
    }
}
