<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Purchase;
use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function index()
    {
        $dateMonthStart= Carbon::now()->startOfMonth()->toDateString('d-m-Y');
        $dateMonthEnd= Carbon::now()->endOfMonth()->toDateString('d-m-Y');
        return Inertia::render('Private/Dashboard/Index',['dateMonthStart'=>$dateMonthStart,'dateMonthEnd'=>$dateMonthEnd]);
    }

    public function dailySales()
    {
        $dailySalesCount = Sale::query()
            ->whereBetween('created_at', [Carbon::now()->startOfDay(),Carbon::now()->endOfDay()])
            ->count();
        if($dailySalesCount<10) {
            $dailySalesCount = str_pad($dailySalesCount, 2, "0", STR_PAD_LEFT);
        }
        $dailySalesAmount = Sale::query()
            ->whereBetween('created_at', [Carbon::now()->startOfDay(),Carbon::now()->endOfDay()])
            ->sum('total');
        return response()->json([
            'dailySalesCount'=> $dailySalesCount,
            'dailySalesAmount'=>round($dailySalesAmount,2)
        ]);
    }
    public function monthlySales(): JsonResponse
    {
        $monthlySalesCount = Sale::query()
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->count();
        if($monthlySalesCount<10) {
            $monthlySalesCount = str_pad($monthlySalesCount, 2, "0", STR_PAD_LEFT);
        }
        $monthlySalesAmount = Sale::query()
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->sum('total');
        return response()->json([
            'monthlySalesCount'=> $monthlySalesCount,
            'monthlySalesAmount'=> round($monthlySalesAmount,2)
        ]);
    }

    public function monthlyPurchases(): JsonResponse
    {
        $monthlyPurchasesCount = Purchase::query()
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->count();
        if($monthlyPurchasesCount<10){
            $monthlyPurchasesCount = str_pad($monthlyPurchasesCount, 2, "0", STR_PAD_LEFT);
        }
        $monthlyPurchaseAmount = Purchase::query()
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->sum('total');
        return response()->json([
            'monthlyPurchaseCount'=> $monthlyPurchasesCount,
            'monthlyPurchaseAmount'=> round($monthlyPurchaseAmount,2)
        ]);
    }

    public function totalCustomers(): JsonResponse
    {
        $totalCustomers = Customer::query()->where('id','<>','1')->count();
        if($totalCustomers<10){
            $totalCustomers = str_pad($totalCustomers, 2, "0", STR_PAD_LEFT);
        }
        return response()->json(['totalCustomers'=> $totalCustomers]);
    }

    public function lowStockProducts()
    {
        $products = DB::table('products')
            ->join('stocks', function ($join) {
                $join->on('products.id', '=', 'stocks.product_id')
                    ->whereColumn('stocks.quantity','<=','products.quantityAlert');
            })
            ->select('products.name', 'stocks.quantity AS stock', 'products.quantityAlert')
            ->get()->toArray();
        return response()->json(['products'=> $products]);
    }
}
