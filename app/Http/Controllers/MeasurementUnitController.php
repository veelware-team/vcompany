<?php

namespace App\Http\Controllers;

use App\Models\MeasurementUnit;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MeasurementUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:measurementUnits.index')->only('index');
        $this->middleware('can:measurementUnits.new')->only('store');
        $this->middleware('can:measurementUnits.update')->only('update');
        $this->middleware('can:measurementUnits.destroy')->only('destroy');
    }

    public function index()
    {
        return Inertia::render('Private/MeasurementUnits/Index');
    }

    public function list(){
        $measurementUnits = MeasurementUnit::query()->select()->orderByDesc('id')->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'name'=> $object->name,
                'shortName'=> $object->shortName,
            ];
        });
        return response()->json(['measurementUnits'=>$measurementUnits]);
    }

    public function store(Request $request)
    {
        $measurementUnitsInstance = MeasurementUnit::create($request->only([
            'name','shortName'
        ]));
        return response()->json(['message'=>'La unidad de medida ha sido guardada exitosamente.']);
    }

    public function update(Request $request, String $measurementUnitId)
    {
        $measurementUnit = MeasurementUnit::find($measurementUnitId);
        $measurementUnit->update($request->only([
            'name','shortName'
        ]));
        return response()->json(['message'=>'La unidad de medida ha sido actualizada exitosamente']);
    }

    public function destroy(String $measurementUnitId)
    {
        $measurementUnit = MeasurementUnit::find($measurementUnitId);
        $measurementUnit->delete();
        return response()->json(['message'=>'La unidad de medida ha sido actualizada exitosamente']);
    }
}
