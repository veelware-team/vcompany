<?php

namespace App\Http\Controllers;

use App\Models\ProductBrand;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductBrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:productBrands.index')->only('index');
        $this->middleware('can:productBrands.new')->only('store');
        $this->middleware('can:productBrands.update')->only('update');
        $this->middleware('can:productBrands.destroy')->only('destroy');
    }

    public function index()
    {
        return Inertia::render('Private/ProductBrands/Index');
    }

    public function list()
    {
        $productBrands = ProductBrand::query()->select()->orderByDesc('id')->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'name'=> $object->name,
                'description'=> $object->description,
            ];
        });
        return response()->json(['productBrands'=>$productBrands]);
    }

    public function store(Request $request)
    {
        $productBrandInstance = ProductBrand::create($request->only([
            'name','description'
        ]));
        return response()->json(['message'=>'La marca de producto ha sido guardada exitosamente.']);
    }

    public function update(Request $request, String $productBrandId)
    {
        $productBrand = ProductBrand::find($productBrandId);
        $productBrand->update($request->only([
            'name','description'
        ]));
        return response()->json(['message'=>'La marca de producto ha sido actualizada exitosamente']);
    }

    public function destroy(String $productBrandInd)
    {
        $productBrand = ProductBrand::find($productBrandInd);
        $productBrand->delete();
        return response()->json(['message'=>'La marca de producto ha sido eliminada exitosamente']);
    }
}

