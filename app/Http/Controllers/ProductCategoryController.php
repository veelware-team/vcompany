<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:productCategories.index')->only('index');
        $this->middleware('can:productCategories.new')->only('store');
        $this->middleware('can:productCategories.update')->only('update');
        $this->middleware('can:productCategories.destroy')->only('destroy');
    }
    public function index()
    {
        return Inertia::render('Private/ProductCategories/Index');
    }

    public function list(){
        $productCategories = ProductCategory::query()->select()->orderByDesc('id')->get()->map(function ($object){
            return [
              'id'=> $object->id,
              'name'=> $object->name,
              'description'=> $object->description,
            ];
        });
        return response()->json(['productCategories'=> $productCategories]);
    }

    public function store(Request $request)
    {
        $productCategoryInstance = ProductCategory::create($request->only([
            'name',
            'description'
        ]));
        return response()->json(['message'=>' La categoria de producto ha sido guardada exitosamente.']);
    }

    public function update(Request $request, String $productCategoryId)
    {
        $productCategory = ProductCategory::find($productCategoryId);
        $productCategory->update($request->only([
            'name',
            'description'
        ]));

        return response()->json(['message'=>'La categoria de producto ha sido actualizada exitosamente.']);
    }

    public function destroy(String $productCategoryId)
    {
        $productCategory = ProductCategory::find($productCategoryId);
        $productCategory->delete();
        return response()->json(['message'=>'La categoria de producto ha sido eliminada exitosamente.']);
    }
}
