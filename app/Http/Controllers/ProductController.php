<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\productCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:products.index')->only('index');
        $this->middleware('can:products.new')->only('store');
        $this->middleware('can:products.update')->only('update');
        $this->middleware('can:products.destroy')->only('destroy');
    }

    public function index()
    {

        return Inertia::render('Private/Products/Index');
    }

    public function generateCode(Request $request){
        $quantity = Product::query()->where('productBrand_id','=',$request->productBrand_id)->count()+1;
        $correlative = str_pad($quantity, 3, "0", STR_PAD_LEFT);

        $brand = ProductBrand::query()->where('id','=',$request->productBrand_id)->first()->only('name');
        $brandShort = mb_strtoupper(mb_substr($brand['name'],0,3));

        $nameShort = '';
        $arrayName = explode(' ', mb_strtoupper($request->name));
        $sizeArray =count($arrayName);
        $i = 0;
        foreach($arrayName as $word){
            $line = true;
            if( $word !=='DE' && $word !== "CON" && $word !== "NO" && $word !== "PARA" && $word !== "LAS" && $word !== "LOS"){
                if($word=='NUMERO' || $word=='NÚMERO'){
                    $nameShort .='NUM';
                }else{
                    $nameShort .=  mb_substr($word,0,4);
                }
            }else{
                $line = false;
            }
            $i = $i+1;
            if($i != $sizeArray && $line==true){
                $nameShort .= '-';
            }
        }
        $code = 'P-'.$correlative.'-'.$brandShort.'-'.$nameShort;
        return response()->json(['code'=>$code]);
    }

    public function list(){
        $products = Product::query()->select()->orderByDesc('id')->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'name'=> $object->name,
                'internalCode'=> $object->internalCode,
                'description'=> $object->description,
                'salePrice'=> $object->salePrice,
                'purchasePrice'=> $object->purchasePrice,
                'discountEnabled'=> $object->discountEnabled,
                'quantityAlert'=> $object->quantityAlert,
                'economicGains'=> $object->economicGains,
                'productCategory_id'=> $object->category->id,
                'categoryName'=> $object->category->name,
                'productBrand_id'=> $object->brand->id,
                'brandName'=> $object->brand->name,
                'productGroup_id'=> $object->group->id,
                'groupName'=> $object->group->name,
                'measurementUnit_id'=> $object->measurementUnit->id,
                'image_url' => url($object->image->url)
            ];
        });
        return response()->json(['products'=>$products]);
    }

    public function listDense(){
        $products = Product::query()->select()->orderByDesc('id')->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'name'=> $object->name,
                'internalCode'=> $object->internalCode,
                'image_url' => url($object->image->url),
                'salePrice'=> $object->salePrice,
                'purchasePrice'=> $object->purchasePrice,
                'discountEnabled'=> $object->discountEnabled,
                'productCategory_id'=> $object->category->id,
                'categoryName'=> $object->category->name,
                'productGroup_id'=> $object->group->id,
                'groupName'=> $object->group->name,
                'quantity'=> $object->stockInfo()['quantity'],
                'state'=> $object->stockInfo()['state'],
            ];
        });
        return response()->json(['products'=>$products]);
    }

    public function store(Request $request)
    {

        $request->merge([
            'discountEnabled' => $request->discountEnabled == "true"? 1 : 0,
            'purchasePrice' => !$request->purchasePrice ? 0.00 : $request->purchasePrice
        ]);

        /*if(!$request->purchasePrice){
            dd('nosta');
            $request->merge([
                'purchasePrice' => 0.00
            ]);
        }else{
            dd('sistaa');
        }*/

        $product = Product::create($request->only([
            'name','internalCode','description','salePrice','purchasePrice','discountEnabled', 'quantityAlert',
            'economicGains','productCategory_id','productBrand_id','productGroup_id','measurementUnit_id'
        ]));

        $url = '/images/default-product.png';
        if ($request->hasFile('image')){
            $url = Storage::put('products',$request->file('image'));
        }
        $product->image()->create(['url'=> $url]);

        if($request->booleanInitalQuantity==true){
            $product->stock()->create(['quantity'=> $request->initialQuantity]);
        }
        return response()->json(['message'=>'El producto ha sido guardado exitosamente.']);
    }

    public function update(Request $request, String $productId)
    {

        $request->merge([
            'discountEnabled' => $request->discountEnabled == "true"? 1 : 0,
        ]);



        $product = Product::find($productId);
        $product->update($request->only([
            'name','internalCode','description','salePrice','purchasePrice','discountEnabled', 'quantityAlert',
            'economicGains','productCategory_id','productBrand_id','productGroup_id','measurementUnit_id'
        ]));
        if($request->changeExistImage == "true"){
            if( Str::contains($product->image->url,'products')){
                Storage::delete($product->image->url);
            }
            $product->image()->delete();
            $url = '/images/default-product.png';
            if ($request->hasFile('image')){
                $url = Storage::put('products',$request->file('image'));
            }
            $product->image()->create(['url'=> $url]);
        }
        return response()->json(['message'=>'El producto ha sido actualizado exitosamente.']);
    }

    public function destroy(String $productId)
    {
        $product = Product::find($productId);
        $product->stock()->delete();
        if( Str::contains($product->image->url,'products')){
            Storage::delete($product->image->url);
        }
        $product->image()->delete();
        $product->delete();
        return response()->json(['message'=>'El producto ha sido eliminado exitosamente']);
    }

}
