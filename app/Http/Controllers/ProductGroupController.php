<?php

namespace App\Http\Controllers;

use App\Models\ProductGroup;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:productGroups.index')->only('index');
        $this->middleware('can:productGroups.new')->only('store');
        $this->middleware('can:productGroups.update')->only('update');
        $this->middleware('can:productGroups.destroy')->only('destroy');
    }
    public function index()
    {
        return Inertia::render('Private/ProductGroups/Index');
    }

    public function list(){
        $productGroups = ProductGroup::query()->select()->orderByDesc('id')->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'name'=> $object->name,
                'description'=> $object->description,
            ];
        });
        return response()->json(['productGroups'=> $productGroups]);
    }

    public function store(Request $request)
    {
        $productGroupInstance = ProductGroup::create($request->only([
            'name',
            'description'
        ]));
        return response()->json(['message'=>' El grupo de productos ha sido guardado exitosamente.']);
    }

    public function update(Request $request, String $productGroupId)
    {
        $productGroup = ProductGroup::find($productGroupId);
        $productGroup->update($request->only([
            'name',
            'description'
        ]));

        return response()->json(['message'=>'El grupo de productos ha sido actualizado exitosamente.']);
    }

    public function destroy(String $productGroupId)
    {
        $productGroup = ProductGroup::find($productGroupId);
        $productGroup->delete();
        return response()->json(['message'=>'El grupo de productos ha sido eliminado exitosamente.']);
    }
}
