<?php

namespace App\Http\Controllers;

use App\Models\PaymentType;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Sale;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:purchases.index')->only('index');
        $this->middleware('can:purchases.new')->only('store');
        $this->middleware('can:purchases.show')->only('purchaseDetail');
        $this->middleware('can:purchases.cancel')->only('cancelPurchase');
    }

    public function index()
    {
        $paymentTypes = PaymentType::query()->select()->where('enabled','=',true)->get();
        return Inertia::render('Private/Purchases/Index',['paymentTypes'=>$paymentTypes]);
    }

    public function store(Request $request)
    {

        //crear la nueva venta
        $purchase = Purchase::create([
            'correlative'=> Purchase::correlative(),
            'paymentType_id'=>$request->paymentType,
            'supplier_id'=>$request->supplier['id'],
            'employee_id'=> $request->user()->id,
            'itemsQuantity'=> $request->itemsQuantity,
            'subtotal'=>$request->subtotalPurchase,
            'discount'=>$request->discount,
            'amountDiscount'=>$request->amountDiscount,
            'total'=>$request->totalPurchase,
        ]);

        foreach ($request->products as $item){
            $tempProduct = Product::find($item['id']);
            if($tempProduct->stock){
                $newStock = $tempProduct->stock->calculateNewQuantity($item['quantity'],"add");
                $tempProduct->stock()->update(['quantity'=>$newStock]);
            }else{
                $tempProduct->stock()->create(['quantity'=>$item['quantity']]);
            }
            //crear detalles de venta
            $purchase->purchaseDetail()->create(['product_id'=>$tempProduct->id,'quantity'=>$item['quantity'],'price'=>$item['price'],'total'=>$item['total']]);
        }
        return response()->json(['message'=>'La compra ha sido generada exitosamente.']);
    }

    public function purchaseDetail(Purchase $purchase)
    {
        $purchaseDetail = PurchaseDetail::query()->with('product')->where('purchase_id','=',$purchase->id)->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'quantity'=> $object->quantity,
                'price'=> $object->price,
                'total'=> $object->total,
                'product_name'=> $object->product->name,
                'product_price'=> $object->product->purchasePrice,
            ];
        });
        return response()->json(['purchaseDetail'=>$purchaseDetail]);
    }

    public function cancelPurchase(Request $request)
    {
        $purchase = Purchase::query()->with('purchaseDetail')->where('id','=',$request->purchaseId)->first();
        foreach ($purchase->purchaseDetail as $item){
            $tempProduct = Product::find($item['product_id']);
            $newStock = $tempProduct->stock->calculateNewQuantity($item['quantity'],"substract");
            //quitar productos del inventario
            $tempProduct->stock()->update(['quantity'=>$newStock]);
        }
        $purchase->update(['canceled'=>true]);
        return response()->json(['message'=>'La compra ha sido cancelada exitosamente.']);
    }
}
