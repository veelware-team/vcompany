<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Purchase;
use App\Models\Sale;
use App\Models\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:reports.stock')->only('stock');
        $this->middleware('can:reports.salesHistory')->only('salesHistory');
        $this->middleware('can:reports.purchasesHistory')->only('purchasesHistory');
    }

    public function stock(){
        return Inertia::render('Private/Reports/Stocks');
    }

    public function listStock(){
        $stocks = Product::query()->orderByDesc('id')->get()->map(function ($object){
            return [
                'internalCode'=> $object->internalCode,
                'categoryName'=> $object->category->name,
                'brandName'=> $object->brand->name,
                'groupName'=> $object->group->name,
                'name'=> $object->name,
                'salePrice'=> $object->salePrice,
                'purchasePrice'=> $object->purchasePrice,
                'measurementUnitName'=> $object->measurementUnit->shortName!=null? $object->measurementUnit->shortName :$object->measurementUnit->name,
                'stock'=> $object->stock?$object->stock->quantity:0,
            ];
        });
        return response()->json(['stocks'=> $stocks]);
    }

    public function salesHistory(Request $request)
    {
        $searchDateFrom = Carbon::now()->toDateString('d-m-Y');
        $searchDateTo = Carbon::now()->toDateString('d-m-Y');
        $searchState = "1";
        $arrayState = [0,1];
        if($request->searchDateFrom){
            $searchDateFrom = $request->searchDateFrom;
        }
        if($request->searchDateTo){
            $searchDateTo = $request->searchDateTo;
        }
        if($request->searchState){
            $searchState = $request->searchState;
            if($searchState == 3){
                $arrayState = [1];
            }
            if($searchState == 2){
                $arrayState = [0];
            }
        }

        $sales = Sale::query()
                 ->with(['paymentType','customer', 'employee'])
                 ->whereBetween('created_at', [$searchDateFrom." 00:00:00",$searchDateTo." 23:59:59"])
                 ->whereIn('canceled',$arrayState)
                 ->orderByDesc('id')
                 ->paginate(5);

        $totalSales = Sale::query()
            ->whereBetween('created_at', [$searchDateFrom." 00:00:00",$searchDateTo." 23:59:59"])
            ->whereIn('canceled',$arrayState)
            ->sum('total');

        $numberOfSales = Sale::query()
            ->whereBetween('created_at', [$searchDateFrom." 00:00:00",$searchDateTo." 23:59:59"])
            ->whereIn('canceled',$arrayState)
            ->count();

        return Inertia::render('Private/Reports/historialVentas', [
            'sales' => $sales,
            'data_searchDateFrom' => $searchDateFrom,
            'data_searchDateTo' => $searchDateTo,
            'data_searchState' => $searchState,
            'numberOfSales' => $numberOfSales,
            'totalSales' => round($totalSales,2) ,
        ]);
    }

    public function purchasesHistory(Request $request){
        $searchDateFrom = Carbon::now()->toDateString('d-m-Y');
        $searchDateTo = Carbon::now()->toDateString('d-m-Y');
        $searchState = "1";
        $arrayState = [0,1];
        if($request->searchDateFrom){
            $searchDateFrom = $request->searchDateFrom;
        }
        if($request->searchDateTo){
            $searchDateTo = $request->searchDateTo;
        }
        if($request->searchState){
            $searchState = $request->searchState;
            if($searchState == 3){
                $arrayState = [1];
            }
            if($searchState == 2){
                $arrayState = [0];
            }
        }

        $purchases = Purchase::query()
            ->with(['paymentType','supplier', 'employee'])
            ->whereBetween('created_at', [$searchDateFrom." 00:00:00",$searchDateTo." 23:59:59"])
            ->whereIn('canceled',$arrayState)
            ->orderByDesc('id')
            ->paginate(5);

        $totalPurchases = Purchase::query()
            ->whereBetween('created_at', [$searchDateFrom." 00:00:00",$searchDateTo." 23:59:59"])
            ->whereIn('canceled',$arrayState)
            ->sum('total');

        $numberOfPurchases = Purchase::query()
            ->whereBetween('created_at', [$searchDateFrom." 00:00:00",$searchDateTo." 23:59:59"])
            ->whereIn('canceled',$arrayState)
            ->count();

        return Inertia::render('Private/Reports/PurchasesH', [
            'purchases' => $purchases,
            'data_searchDateFrom' => $searchDateFrom,
            'data_searchDateTo' => $searchDateTo,
            'data_searchState' => $searchState,
            'numberOfPurchases' => $numberOfPurchases,
            'totalPurchases' => round($totalPurchases,2),
        ]);
    }
}
