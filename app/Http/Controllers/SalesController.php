<?php

namespace App\Http\Controllers;

use App\Models\PaymentType;
use App\Models\Product;
use App\Models\Sale;
use App\Models\SaleDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:sales.index')->only('index');
        $this->middleware('can:sales.new')->only('store');
        $this->middleware('can:sales.show')->only('saleDetail');
        $this->middleware('can:sales.cancel')->only('cancelSale');
    }

    public function index()
    {
        $paymentTypes = PaymentType::query()->select()->where('enabled','=',true)->get();
        return Inertia::render('Private/Sales/Index',['paymentTypes'=>$paymentTypes]);
    }

    public function store(Request $request)
    {
        foreach ($request->products as $item){
            $tempProduct = Product::find($item['id']);
            $newStock = $tempProduct->stock->calculateNewQuantity($item['quantity'],"substract");
            if($newStock < 0){
                return response()->json([
                    'productId'=>$item['id'],
                    'message'=>'Otro usuario en el sistema ha modificado la cantidad disponible para el producto: '.$tempProduct->name.' por favor vuelve a agregarlo.'
                ],202);
            }
        }
        //crear la nueva venta
        $sale = Sale::create([
            'correlative'=> Sale::correlative(),
            'paymentType_id'=>$request->paymentType,
            'customer_id'=>$request->customer['id'],
            'employee_id'=> $request->user()->id,
            'itemsQuantity'=> $request->itemsQuantity,
            'subtotal'=>$request->subtotalSale,
            'discount'=>$request->discount,
            'amountDiscount'=>$request->amountDiscount,
            'total'=>$request->totalSale,
        ]);

        foreach ($request->products as $item){
            $tempProduct = Product::find($item['id']);
            $newStock = $tempProduct->stock->calculateNewQuantity($item['quantity'],"substract");
            //descontar productos del inventario
            $tempProduct->stock()->update(['quantity'=>$newStock]);
            //crear detalles de venta
            $sale->saleDetail()->create(['product_id'=>$tempProduct->id,'quantity'=>$item['quantity'],'price'=>$item['price'],'total'=>$item['total']]);
        }
        return response()->json(['message'=>'La venta ha sido generada exitosamente.']);
    }

    public function saleDetail(Sale $sale)
    {
        $saleDetail = SaleDetail::query()->with('product')->where('sale_id','=',$sale->id)->get()->map(function ($object){
            return [
                'id'=> $object->id,
                'quantity'=> $object->quantity,
                'price'=> $object->price,
                'total'=> $object->total,
                'product_name'=> $object->product->name,
                'product_price'=> $object->product->salePrice,
            ];
        });
        return response()->json(['saleDetail'=>$saleDetail]);
    }

    public function cancelSale(Request $request)
    {
        $sale = Sale::query()->with('saleDetail')->where('id','=',$request->saleId)->first();
        foreach ($sale->saleDetail as $item){
            $tempProduct = Product::find($item['product_id']);
            $newStock = $tempProduct->stock->calculateNewQuantity($item['quantity'],"add");
            //agregar productos al inventario
            $tempProduct->stock()->update(['quantity'=>$newStock]);
        }
        $sale->update(['canceled'=>true]);
        return response()->json(['message'=>'La venta ha sido cancelada exitosamente.']);
    }
}
