<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:suppliers.index')->only('index');
        $this->middleware('can:suppliers.new')->only('store');
        $this->middleware('can:suppliers.update')->only('update');
        $this->middleware('can:suppliers.destroy')->only('destroy');
    }
    public function index()
    {
        return Inertia::render('Private/Suppliers/Index');
    }

    public function list(){
        $suppliers = Supplier::query()->select()->where('id','<>','1')->orderBy('id','desc')->get()->map(function ($objectTemp){
            return [
                'id'=>$objectTemp->id,
                'name'=>$objectTemp->name,
                'company'=>$objectTemp->company,
                'phone'=>$objectTemp->phone,
                'email'=>$objectTemp->email,
                'nit'=>$objectTemp->nit,
                'adress'=>$objectTemp->adress,
            ];
        });
        return response()->json(['suppliers'=>$suppliers]);
    }

    public function store(Request $request)
    {
        $customerInstance = Supplier::create($request->only([
            'name',
            'company',
            'phone',
            'email',
            'nit',
            'adress',
        ]));
        return response()->json(['message'=>'El proveedor se ha guardado exitosamente.']);
    }

    public function update(Request $request, String $supplierId)
    {
        $customer = Supplier::find($supplierId);
        $customer->update($request->only([
            'name',
            'company',
            'phone',
            'email',
            'nit',
            'adress',
        ]));
        return response()->json(['message'=>'El proveedor se ha actualizado exitosamente.']);
    }

    public function destroy(String $supplierId)
    {
        $customer = Supplier::find($supplierId);
        $customer->delete();
        return response()->json(['message'=>'El proveedor se ha eliminado exitosamente.']);
    }
}
