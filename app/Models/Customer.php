<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];

    public function sale(){
        return $this->belongsTo(Sale::class);
    }

    public function birthDateFormated()
    {
        if ($this->birthdate){
            $dia   = Carbon::parse($this->birthdate)->translatedFormat('d');
            $mes   = Carbon::parse($this->birthdate)->translatedFormat('F');
            return $dia." de ".$mes;
        }
        return null;
    }
}
