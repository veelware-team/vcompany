<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $guarded = [];

    /** polimorfica uno a muchos Una modelo puede tener muchas imagenes */
    public function modelo(){
        return $this->morphTo();
    }
}
