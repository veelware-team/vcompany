<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeasurementUnit extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];

    public function Products(){
        return $this->belongsTo(Product::class,'id','measurementUnit_id')->withTrashed();
    }
}
