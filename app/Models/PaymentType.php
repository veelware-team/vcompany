<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentType extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];
    public function sale(){
        return $this->belongsTo(Sale::class);
    }
    public function purchase(){
        return $this->belongsTo(Purchase::class);
    }
}
