<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];
    //'discountEnabled'=> 'boolean',
    protected  $casts = ['discountEnabled'=> 'boolean','salePrice' => 'decimal:2','purchasePrice' => 'decimal:2'];

    public function category(){
        return $this->hasOne(ProductCategory::class,'id','productCategory_id');
    }
    public function group(){
        return $this->hasOne(ProductGroup::class,'id','productGroup_id');
    }
    public function brand(){
        return $this->hasOne(ProductBrand::class,'id','productBrand_id');
    }
    public function measurementUnit(){
        return $this->hasOne(MeasurementUnit::class,'id','measurementUnit_id');
    }
    public function stock(){
        return $this->hasOne(Stock::class);
    }

    public function image(){
        return $this->morphOne(Image::class,'modelo');
    }

    public function stockInfo(){
        $data = [
            'quantity'=> 0,
            'state'=> 'STOCK_NOT',
        ];
        if($this->stock){
            $productQuantity = $this->stock->quantity;
            if($productQuantity == 0){
                $data['state'] = 'STOCK_AGT';
            }else if(( $productQuantity <= $this->quantityAlert ) && $productQuantity > 0){
                $data['state'] = 'STOCK_POUNDS';
            }else{
                $data['state'] = 'STOCK_DISP';
            }
            $data['quantity'] = $productQuantity;
        }
        return $data;
    }

}
