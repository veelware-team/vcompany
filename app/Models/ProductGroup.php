<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function Products(){
        return $this->belongsTo(Product::class,'id','productGroup_id');
    }
}
