<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    protected  $guarded =[];
    protected  $dates = ['created_at'];

    public function paymentType(){
        return $this->hasOne(PaymentType::class,'id','paymentType_id')->withTrashed();
    }
    public function purchaseDetail(){
        return $this->hasMany(PurchaseDetail::class);
    }
    public function supplier(){
        return $this->hasOne(Supplier::class,'id','supplier_id')->withTrashed();
    }
    public function employee(){
        return $this->hasOne(User::class,'id','employee_id')->withTrashed();
    }

    public static function correlative(): string
    {
        $numberOfPurchase = Purchase::query()->count();
        if($numberOfPurchase==0){
            return 'CPOS1'.str_pad(intval($numberOfPurchase)+1, 6, "0", STR_PAD_LEFT);
        }else{
            //$lastSale = Sale::query()->orderBy('id','desc')->first()->value('correlative');
            $lastPurchase = Purchase::query()->select('correlative')->orderBy('id','desc')->first();
            $incrementalNumberPurchase = substr($lastPurchase['correlative'],4,1);
            $nCorrelative = substr($lastPurchase['correlative'],5);
            if($nCorrelative =='999999'){
                $incrementalNumberPurchase = intval($incrementalNumberPurchase)+1;
                return 'CPOS'.$incrementalNumberPurchase.str_pad(1, 6, "0", STR_PAD_LEFT);
            }else{
                return 'CPOS'.$incrementalNumberPurchase.str_pad(intval($nCorrelative)+1, 6, "0", STR_PAD_LEFT);
            }
        }
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y h:i a');
    }

}
