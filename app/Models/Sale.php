<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $guarded = [];
   /* protected $casts = [
      'created_at' =>'datetime: d-m-Y h:i a'
    ];*/
    protected $dates = ['created_at'];
    public function paymentType(){
        return $this->hasOne(PaymentType::class,'id','paymentType_id')->withTrashed();
    }
    public function saleDetail(){
        return $this->hasMany(SaleDetail::class);
    }
    public function customer(){
        return $this->hasOne(Customer::class,'id','customer_id')->withTrashed();
    }
    public function employee(){
        return $this->hasOne(User::class,'id','employee_id')->withTrashed();
    }

    public static function correlative(): string
    {
        $numberOfSales = Sale::query()->count();
        if($numberOfSales==0){
            return 'VPOS1'.str_pad(intval($numberOfSales)+1, 6, "0", STR_PAD_LEFT);
        }else{
            //$lastSale = Sale::query()->orderBy('id','desc')->first()->value('correlative');
            $lastSale = Sale::query()->select('correlative')->orderBy('id','desc')->first();
            $incrementalNumber = substr($lastSale['correlative'],4,1);
            $nCorrelative = substr($lastSale['correlative'],5);
            if($nCorrelative =='999999'){
                $incrementalNumber = intval($incrementalNumber)+1;
                return 'VPOS'.$incrementalNumber.str_pad(1, 6, "0", STR_PAD_LEFT);
            }else{
                return 'VPOS'.$incrementalNumber.str_pad(intval($nCorrelative)+1, 6, "0", STR_PAD_LEFT);
            }
        }
    }

/*    public function getCreatedAtAttribute($date)
    {

        return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('Y-m-d');
        //return Carbon::now()->format('d-m-Y h:i a');
    }*/

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y h:i a');
    }
}
