<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function sales()
    {
        return $this->belongsTo(Sale::class);
    }

    public function product(){
        return $this->hasOne(Product::class,'id','product_id')->withTrashed();
        //return $this->hasOne(Product::class,'id','product_id')->select(['name','salePrice']);
    }
}
