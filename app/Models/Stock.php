<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function calculateNewQuantity($quantityMinus,$type){
        return $type=="add"?$this->quantity + $quantityMinus: $this->quantity - $quantityMinus;
    }
}
