<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
        'lastname',
        'email',
        'password',
        'active',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userFirstName(){
        $name = explode(" ", $this->name);
        return $name[0];
    }
    //Auth::user()->initials()
    public function initials(){
        $firstName = explode(" ", $this->name );
        $lastName = explode(" ", $this->lastname );
        $initials = mb_substr($firstName[0],0,1) . mb_substr($lastName[0],0,1);
        return strtoupper($initials);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sale(){
        return $this->belongsTo(Sale::class,'id','employee_id');
    }

    public function purchase(){
        return $this->belongsTo(Purchase::class,'id','employee_id');
    }
}
