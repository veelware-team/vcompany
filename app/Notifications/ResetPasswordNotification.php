<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
class ResetPasswordNotification extends Notification
{
    use Queueable;

    public $token;
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url( route('password.reset',[
            'token'=>$this->token,
            'email'=>$notifiable->getEmailForPasswordReset(),
        ],false));

        return (new MailMessage)
            ->theme('theme_valentina')
            ->subject('Restablece tu contraseña en Valentina Company')
            ->greeting('¡Hemos recibido una solicitud para cambiar tu contraseña!')
            ->line('Hola '.$notifiable->userFirstName().',')
            ->line('Para realizar el cambio, por favor haz clic en el boton de abajo.')
            ->action('Restablecer contraseña', $url)
            ->line(Lang::get('Este enlace de restablecimiento de contraseña expirará en :count minutos.', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')]))
            ->line('Si recibiste este mensaje por error por favor ignora este correo.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
