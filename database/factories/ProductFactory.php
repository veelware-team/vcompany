<?php

namespace Database\Factories;

use App\Models\MeasurementUnit;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'internalCode' => $this->faker->slug(4,true),
            'name' => $this->faker->name(),
            'description' => $this->faker->text(100),
            'salePrice' => $this->faker->randomFloat( 2, 4,8),
            'purchasePrice' => $this->faker->randomFloat(2,4,8),
            'discountEnabled' => $this->faker->boolean(50),
            'quantityAlert' => 10,
            'economicGains' => $this->faker->randomFloat(2,4,8),
            'productCategory_id' => ProductCategory::all()->random()->id,
            'productBrand_id' => ProductBrand::all()->random()->id,
            'productGroup_id' => ProductGroup::all()->random()->id,
            'measurementUnit_id' => MeasurementUnit::all()->random()->id,
        ];
    }

    public function configure()
    {
        return $this->afterMaking(function (Product $product) {
            //
        })->afterCreating(function (Product $product) {
            $url = '/images/default-product.png';
            $product->image()->create(['url'=> $url]);
            $product->stock()->create(['quantity'=> 40]);
        });
    }
}
