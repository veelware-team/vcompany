<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sale::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'correlative' => Sale::correlative(),
            'itemsQuantity' => 1,
            'subtotal' => $this->faker->randomFloat( 2, 4,8),
            'discount' => $this->faker->randomFloat( 2, 4,8),
            'amountDiscount' => $this->faker->randomFloat( 2, 4,8),
            'total' => $this->faker->randomFloat( 2, 4,8),
            'paymentType_id' => 1,
            'customer_id' => 1,
            'employee_id' => User::all()->random()->id,
            'canceled' => $this->faker->boolean(50),
        ];
    }
    public function configure()
    {
        return $this->afterMaking(function (Sale $sale) {
            //
        })->afterCreating(function (Sale $sale) {
            $sale->saleDetail()->create([
                'product_id'=> Product::all()->random()->id,
                'quantity'=>1,
                'price'=> 20.00,
                'total'=>20.00
            ]);
        });
    }
}
