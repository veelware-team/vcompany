<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /*•	Nombre*
    •	Código interno (SKU)*
    •	Precio de compra*
    •	Precio de venta*
    •	Habilitado para descuento (si, no) *
    •	Cantidad para reorden y alerta (para avisar que hay pocas unidades)
    •	Estado (Disponible, pocas unidades, Agotado) (automático)
    •	Ganancia inmediata (Campo privado, solo disponible al administrador del negocio) (automático)
    •	Categoría*
    •	Marca*
    •	Grupo*
    •	Unidad de medida*

    $product = Product::create(['name'=>'Producto 1','internalCode'=>'P01-MIA-MON','description'=>'descripcion del producto','salePrice' => 10.50,'purchasePrice' => 20.50,'discountEnabled' => true,'quantityAlert' =>15,'economicGains' => 10.00,'productCategory_id' =>1,'productBrand_id' => 1,'productGroup_id' =>1,'measurementUnit_id' =>1]);
    */

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('internalCode');
            $table->string('name');
            $table->text('description')->nullable();
            $table->double('salePrice',8,2);
            $table->double('purchasePrice',8,2)->default(0.00);
            $table->boolean('discountEnabled');
            $table->integer('quantityAlert');
            $table->double('economicGains',8,2);

            $table->foreignId('productCategory_id')->nullable()
                ->constrained('product_categories','id')
                ->onDelete('set null')->onUpdate('cascade');

            $table->foreignId('productBrand_id')->nullable()
                ->constrained('product_brands','id')
                ->onDelete('set null')->onUpdate('cascade');

            $table->foreignId('productGroup_id')->nullable()
                ->constrained('product_groups','id')
                ->onDelete('set null')->onUpdate('cascade');

            $table->foreignId('measurementUnit_id')->nullable()
                ->constrained('measurement_units','id')
                ->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
