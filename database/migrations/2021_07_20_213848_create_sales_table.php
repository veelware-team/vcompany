<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $sale = Sale::create(['correlative'=>'V001','itemsQuantity'=>3,'subtotal'=>40.00,'discount'=>10.00,'amountDiscount'=>4.00,'total'=>36.00,'paymentType_id'=>1,'customer_id'=>1,'employee_id'=>3]);
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('correlative');
            $table->integer('itemsQuantity');
            $table->double('subtotal',8,2);
            $table->double('discount',8,2);
            $table->double('amountDiscount',8,2);
            $table->double('total',8,2);
            $table->foreignId('paymentType_id')->constrained('payment_types','id');
            $table->foreignId('customer_id');
            $table->foreignId('employee_id')->constrained('users','id');
            $table->boolean('canceled')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
