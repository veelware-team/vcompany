<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->string('correlative');
            $table->integer('itemsQuantity');
            $table->double('subtotal',8,2);
            $table->double('discount',8,2);
            $table->double('amountDiscount',8,2);
            $table->double('total',8,2);
            $table->foreignId('paymentType_id')->constrained('payment_types','id');
            $table->foreignId('supplier_id');
            $table->foreignId('employee_id')->constrained('users','id');
            $table->boolean('canceled')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
