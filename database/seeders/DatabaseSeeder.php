<?php

namespace Database\Seeders;

use App\Models\MeasurementUnit;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\Sale;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(
            [RoleSeeder::class, PaymentTypeSeeder::class, UserSeeder::class]
        );
        /*ProductCategory::factory(15)->create();
        ProductBrand::factory(15)->create();
        ProductGroup::factory(15)->create();
        MeasurementUnit::factory(15)->create();
        Product::factory(150)->create();
        Sale::factory(999)->create();*/
    }
}
