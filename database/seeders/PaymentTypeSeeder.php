<?php

namespace Database\Seeders;

use App\Models\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $efectivo = PaymentType::create(['name'=>'efectivo','enabled'=>true]);
        $tarjeta = PaymentType::create(['name'=>'tarjeta','enabled'=>false]);
    }
}
