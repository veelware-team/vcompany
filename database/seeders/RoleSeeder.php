<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolSuperAdmin = Role::create(['name'=>'superAdmin']);
        $rolAdmin = Role::create(['name'=>'administrador']);
        $rolTrabajador = Role::create(['name'=>'trabajador']);
        Permission::create(['name'=>'private.dashboard.admin','description'=>'Visualizacion Dashboard Administrador'])->syncRoles([$rolAdmin,$rolSuperAdmin]);
        Permission::create(['name'=>'private.dashboard.employee','description'=>'Visualizacion Dashboard Trabajador'])->syncRoles([$rolTrabajador]);

        //Clientes
        Permission::create(['name'=>'customers.index','description'=>'Listado cliente'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'customers.new','description'=>'Crear cliente'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'customers.show','description'=>'Ver cliente'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'customers.update','description'=>'Editar cliente'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'customers.destroy','description'=>'Eliminar cliente'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Proveedores
        Permission::create(['name'=>'suppliers.index','description'=>'Listado proveedores'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'suppliers.new','description'=>'Crear proveedor'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'suppliers.show','description'=>'Ver proveedor'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'suppliers.update','description'=>'Editar proveedor'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'suppliers.destroy','description'=>'Eliminar proveedor'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Categorias de producto
        Permission::create(['name'=>'productCategories.index','description'=>'Listado categorias de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productCategories.new','description'=>'Crear categoria de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'productCategories.show','description'=>'Ver categoria de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productCategories.update','description'=>'Editar categoria de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productCategories.destroy','description'=>'Eliminar categoria de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Marcas de producto
        Permission::create(['name'=>'productBrands.index','description'=>'Listado marcas de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productBrands.new','description'=>'Crear marcas de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'productBrands.show','description'=>'Ver marcas de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productBrands.update','description'=>'Editar marcas de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productBrands.destroy','description'=>'Eliminar marcas de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Grupos de producto
        Permission::create(['name'=>'productGroups.index','description'=>'Listado grupos de productos'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productGroups.new','description'=>'Crear grupo de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'productGroups.show','description'=>'Ver grupo de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productGroups.update','description'=>'Editar grupo de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'productGroups.destroy','description'=>'Eliminar grupo de producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Unidades de medida
        Permission::create(['name'=>'measurementUnits.index','description'=>'Listado unidades de medida'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'measurementUnits.new','description'=>'Crear unidad de medida'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'measurementUnits.show','description'=>'Ver unidad de medida'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'measurementUnits.update','description'=>'Editar unidad de medida'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'measurementUnits.destroy','description'=>'Eliminar unidad de medida'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Productos
        Permission::create(['name'=>'products.index','description'=>'Listado de productos'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'products.new','description'=>'Crear producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'products.show','description'=>'Ver producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'products.update','description'=>'Editar producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'products.destroy','description'=>'Eliminar producto'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Ventas
        Permission::create(['name'=>'sales.index','description'=>'Pantalla de ventas'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'sales.new','description'=>'Realizar venta'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'sales.show','description'=>'Ver venta'])->syncRoles([$rolSuperAdmin,$rolAdmin,$rolTrabajador]);
        Permission::create(['name'=>'sales.cancel','description'=>'Anular venta'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Compras
        Permission::create(['name'=>'purchases.index','description'=>'Pantalla de compras'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'purchases.new','description'=>'Realizar compra'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'purchases.show','description'=>'Ver compra'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'purchases.cancel','description'=>'Anular compra'])->syncRoles([$rolSuperAdmin,$rolAdmin]);

        //Reportes
        Permission::create(['name'=>'reports.stock','description'=>'Inventario'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'reports.salesHistory','description'=>'Historial de ventas'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        Permission::create(['name'=>'reports.purchasesHistory','description'=>'Historial de compras'])->syncRoles([$rolSuperAdmin,$rolAdmin]);
        //Configuraciones
        Permission::create(['name'=>'config.index','description'=>'Configuraciones'])->syncRoles([$rolSuperAdmin]);
    }
}
