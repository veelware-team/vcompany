<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clienteDefault = Customer::create(['name'=>'Cliente Anonimo', 'nit'=>'C/F']);
        $proveedorDefault = Supplier::create(['name'=>'Proveedor Anonimo', 'company'=>'Anonima', 'nit'=>'C/F']);

        $usuarioSuperAdmin = User::create([
            'code' =>'admin',
            'name' =>'Administrador ',
            'lastname' =>'Sistema',
            'email' =>'walter_ehm@hotmail.com',
            'active' => 1,
            'password' => Hash::make('12345'),
            'remember_token' => Str::random(10),
        ]);
        $usuarioSuperAdmin->assignRole('superAdmin');

        $usuarioAdmin = User::create([
            'code' =>'adminvalentina',
            'name' =>'Atzy',
            'lastname' =>'Veliz',
            'email' =>'walter_ehm@hotmail.com',
            'active' => 1,
            'password' => Hash::make('atzyvalentina2021'),
            'remember_token' => Str::random(10),
        ]);
        $usuarioAdmin->assignRole('administrador');

        $usuarioTrabajador1 = User::create([
            'code' =>'vendedor1',
            'name' =>'Vendedor 1',
            'lastname' =>' 1 ',
            'email' =>'walter_ehm@hotmail.com',
            'active' => 1,
            'password' => Hash::make('12345'),
            'remember_token' => Str::random(10),
        ]);
        $usuarioTrabajador1->assignRole('trabajador');
        $usuarioTrabajador1->givePermissionTo(['products.index','products.new', 'products.show']);


        $usuarioTrabajador2 = User::create([
            'code' =>'vendedor2',
            'name' =>'Vendedor 2',
            'lastname' =>'2',
            'email' =>'walter_ehm@hotmail.com',
            'active' => 1,
            'password' => Hash::make('12345'),
            'remember_token' => Str::random(10),
        ]);
        $usuarioTrabajador2->assignRole('trabajador');
    }
}
