(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Private_Products_Index_vue"],{

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_croppa_dist_vue_croppa_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-croppa/dist/vue-croppa.css */ "./node_modules/vue-croppa/dist/vue-croppa.css");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Index",
  metaInfo: {
    title: 'Catalogo de productos'
  },
  data: function data() {
    return {
      menu: false,
      loadTable: true,
      snackbar: false,
      snackbar_text: '',
      snackbar_color: 'error',
      timeout: 5000,
      search: '',
      dialog: false,
      dialogDelete: false,
      viewOnly: false,
      rules: {
        required: function required(value) {
          return !!value || 'Debes llenar este campo.';
        },
        price: function price(value) {
          var pattern = /^[0-9]*\.[0-9]{2}$/;
          return pattern.test(value) || 'Precio invalido.';
        },
        number: function number(value) {
          var pattern = /^[0-9]*$/;
          return pattern.test(value) || 'Debes de ingresar un número.';
        }
      },
      products: [],
      categories: [],
      brands: [],
      groups: [],
      measurementUnits: [],
      editedIndex: -1,
      editedItem: {
        'id': '',
        'name': '',
        'internalCode': '',
        'description': '',
        'salePrice': '',
        'purchasePrice': '',
        'discountEnabled': '',
        'quantityAlert': '',
        'economicGains': '',
        'productCategory_id': '',
        'categoryName': '',
        'productBrand_id': '',
        'brandName': '',
        'productGroup_id': '',
        'groupName': '',
        'measurementUnit_id': '',
        'booleanInitalQuantity': 0,
        'initialQuantity': '',
        'image_url': ''
      },
      defaultItem: {
        'id': '',
        'name': '',
        'internalCode': '',
        'description': '',
        'salePrice': '',
        'purchasePrice': '',
        'discountEnabled': false,
        'quantityAlert': '',
        'economicGains': '',
        'productCategory_id': '',
        'categoryName': '',
        'productBrand_id': '',
        'brandName': '',
        'productGroup_id': '',
        'groupName': '',
        'measurementUnit_id': '',
        'booleanInitalQuantity': 0,
        'initialQuantity': '',
        'image_url': ''
      },
      defaultHybridtItem: {
        'name': '',
        'description': '',
        'shortName': ''
      },
      typeHybridDialog: '',
      hybridDialog: false,
      image: null,
      changingImage: false,
      url: '/images/default-product.png',
      myCroppa: {},
      urlTemp: '',
      changeExistImage: false
    };
  },
  computed: {
    formTitle: function formTitle() {
      return this.editedIndex === -1 ? 'Nuevo producto' : this.viewOnly ? 'Datos del producto' : 'Editar producto';
    },
    formHybridTitle: function formHybridTitle() {
      var title = '';

      if (this.typeHybridDialog === 'category') {
        title = 'Nueva categoria';
      } else if (this.typeHybridDialog === 'group') {
        title = 'Nuevo grupo';
      } else if (this.typeHybridDialog === 'brand') {
        title = 'Nueva marca';
      } else if (this.typeHybridDialog === 'measurementUnit') {
        title = 'Nueva unidad de medida';
      }

      return title;
    },
    headers: function headers() {
      if (this.$page.props.auth.user.permission.includes('private.dashboard.admin')) {
        return [
        /*sortable: false,*/
        {
          text: 'Codigo',
          value: 'internalCode',
          align: 'start'
        }, {
          text: 'Nombre',
          value: 'name'
        }, {
          text: 'Categoria',
          value: 'categoryName'
        }, {
          text: 'Grupo',
          value: 'groupName'
        }, {
          text: 'Marca',
          value: 'brandName'
        }, {
          text: 'Precio Compra',
          value: 'purchasePrice'
        }, {
          text: 'Precio Venta',
          value: 'salePrice'
        }, {
          text: 'Descuento',
          value: 'discountEnabled'
        }, {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }];
      } else {
        return [
        /*sortable: false,*/
        {
          text: 'Codigo',
          value: 'internalCode',
          align: 'start'
        }, {
          text: 'Nombre',
          value: 'name'
        }, {
          text: 'Categoria',
          value: 'categoryName'
        }, {
          text: 'Grupo',
          value: 'groupName'
        }, {
          text: 'Marca',
          value: 'brandName'
        }, {
          text: 'Precio Venta',
          value: 'salePrice'
        }, {
          text: 'Descuento',
          value: 'discountEnabled'
        }, {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }];
      }
    }
  },
  created: function created() {
    var _this = this;

    this.getProducts();
    setTimeout(function () {
      return _this.getCategoriesSelect();
    }, 300);
    setTimeout(function () {
      return _this.getBrandsSelect();
    }, 300);
    setTimeout(function () {
      return _this.getGroupsSelect();
    }, 300);
    setTimeout(function () {
      return _this.getMeasurementsUnitsSelect();
    }, 300);
  },
  methods: {
    handleChangeExistedImage: function handleChangeExistedImage() {
      this.changingImage = true;
      this.changeExistImage = true;
      this.urlTemp = this.editedItem.image_url;
    },
    handleCancelChangeExistedImage: function handleCancelChangeExistedImage() {
      this.changingImage = false;
      this.changeExistImage = false;
      this.editedItem.image_url = this.urlTemp;
      this.url = this.urlTemp;
      this.myCroppa.remove();
    },
    getImageResize: function getImageResize() {
      var _this2 = this;

      var hasImage = this.myCroppa.generateDataUrl();

      if (!hasImage) {
        this.openSnack("Debes seleccionar una imagen antes de guardar.", "error");
      } else {
        this.myCroppa.generateBlob(function (blob) {
          _this2.url = URL.createObjectURL(blob);
          _this2.image = blob;
          _this2.changingImage = false;
        }, 'image/jpeg', 0.8);
        this.myCroppa.remove();
      }
    },
    calculateEconomicGains: function calculateEconomicGains() {
      if (this.editedItem.salePrice !== '' && this.editedItem.purchasePrice !== '') {
        this.editedItem.economicGains = parseFloat(parseFloat(this.editedItem.salePrice).toFixed(2) - parseFloat(this.editedItem.purchasePrice).toFixed(2)).toFixed(2);

        if (this.editedItem.economicGains < 0) {
          this.openSnack("El precio de venta debe de ser mayor al de compra.", "error");
          this.editedItem.economicGains = '';
          this.editedItem.salePrice = '';
        }
      } else {
        this.editedItem.economicGains = 0;
      }
    },
    generateCode: function generateCode() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (_this3.editedItem.name && _this3.editedItem.productBrand_id) {
                  axios.post(route('products.generateCode'), _this3.editedItem).then(function (response) {
                    _this3.editedItem.internalCode = response.data.code;
                  })["catch"](function (error) {
                    _this3.openSnack("ha ocurrido un error al momento de generar el codigo del producto, por favor comuniquese con el administrador del sistema.", "error");

                    console.log(error);
                  });
                }

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    openHybridDialog: function openHybridDialog(type) {
      this.typeHybridDialog = type;
      this.hybridDialog = true;
    },
    closeHybridDialog: function closeHybridDialog() {
      var _this4 = this;

      this.$refs.formHybridDialog.resetValidation();
      this.hybridDialog = false;
      this.defaultHybridtItem.name = '';
      this.defaultHybridtItem.description = '';
      this.defaultHybridtItem.shortName = '';
      setTimeout(function () {
        return _this4.typeHybridDialog = '';
      }, 100);
    },
    saveHybridItem: function saveHybridItem() {
      var url = '';

      if (this.typeHybridDialog === 'category') {
        url = 'productCategories.store';
      } else if (this.typeHybridDialog === 'group') {
        url = 'productGroups.store';
      } else if (this.typeHybridDialog === 'brand') {
        url = 'productBrands.store';
      } else if (this.typeHybridDialog === 'measurementUnit') {
        url = 'measurementUnits.store';
      }

      if (this.$refs.formHybridDialog.validate()) {
        this.saveHybrid(url);
      }
    },
    saveHybrid: function saveHybrid(url) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                axios.post(route(url), _this5.defaultHybridtItem).then(function (response) {
                  _this5.openSnack(response.data.message, "success");

                  if (_this5.typeHybridDialog === 'category') {
                    _this5.getCategoriesSelect();
                  } else if (_this5.typeHybridDialog === 'group') {
                    _this5.getGroupsSelect();
                  } else if (_this5.typeHybridDialog === 'brand') {
                    _this5.getBrandsSelect();
                  } else if (_this5.typeHybridDialog === 'measurementUnit') {
                    _this5.getMeasurementsUnitsSelect();
                  }

                  _this5.closeHybridDialog();
                })["catch"](function (error) {
                  _this5.openSnack("ha ocurrido un error al momento de agregar el elemento.", "error");

                  _this5.closeHybridDialog();

                  console.log(error);
                });

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },

    /*Notificaciones*/
    openSnack: function openSnack(text, color) {
      this.snackbar_text = text;
      this.snackbar_color = color;
      this.snackbar = true;
    },
    deleteItemConfirm: function deleteItemConfirm() {
      this.deleteProduct();
      this.closeDialog('dialogDelete');
    },
    closeDialog: function closeDialog(typeDialog) {
      var _this6 = this;

      typeDialog === "dialogDelete" ? this.dialogDelete = false : this.dialog = false;
      setTimeout(function () {
        return _this6.reset();
      }, 500);
    },
    reset: function reset() {
      this.viewOnly = false;
      this.editedItem = Object.assign({}, this.defaultItem);
      this.$refs.formModal.resetValidation();
      this.editedIndex = -1;
      this.image = '';
      this.url = '/images/default-product.png';
      this.changeExistImage = false;
      this.urlTemp = '';
    },
    assignItem: function assignItem(item, typeDialog, mode) {
      this.editedIndex = this.products.indexOf(item);
      this.editedItem = Object.assign({}, item);
      typeDialog === "dialogDelete" ? this.dialogDelete = true : this.dialog = true;
      mode === "show" ? this.viewOnly = true : this.viewOnly = false;
      this.url = this.editedItem.image_url;
      this.changeExistImage = false;
      this.urlTemp = '';
    },
    save: function save() {
      if (this.$refs.formModal.validate()) {
        if (this.editedIndex > -1) {
          this.updateProduct();
          this.closeDialog('dialogEdit');
        } else {
          this.saveProduct();
          this.closeDialog('dialogEdit');
        }
      }
    },
    // *********** api calls ***********
    getCategoriesSelect: function getCategoriesSelect() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios.get(route('productCategories.list'));

              case 3:
                response = _context3.sent;
                _this7.categories = response.data.productCategories;
                _context3.next = 10;
                break;

              case 7:
                _context3.prev = 7;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 10:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 7]]);
      }))();
    },
    getBrandsSelect: function getBrandsSelect() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios.get(route('productBrands.list'));

              case 3:
                response = _context4.sent;
                _this8.brands = response.data.productBrands;
                _context4.next = 10;
                break;

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }))();
    },
    getGroupsSelect: function getGroupsSelect() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee5() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios.get(route('productGroups.list'));

              case 3:
                response = _context5.sent;
                _this9.groups = response.data.productGroups;
                _context5.next = 10;
                break;

              case 7:
                _context5.prev = 7;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 10:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 7]]);
      }))();
    },
    getMeasurementsUnitsSelect: function getMeasurementsUnitsSelect() {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee6() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return axios.get(route('measurementUnits.list'));

              case 3:
                response = _context6.sent;
                _this10.measurementUnits = response.data.measurementUnits;
                _context6.next = 10;
                break;

              case 7:
                _context6.prev = 7;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 10:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 7]]);
      }))();
    },
    // CORE DE PRODUCTOS
    getProducts: function getProducts() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee7() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return axios.get(route('products.list'));

              case 3:
                response = _context7.sent;
                _this11.products = response.data.products;
                _this11.loadTable = false;
                _context7.next = 11;
                break;

              case 8:
                _context7.prev = 8;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 11:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 8]]);
      }))();
    },
    saveProduct: function saveProduct() {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee8() {
        var bodyFormData, key;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                bodyFormData = new FormData();
                bodyFormData.append('image', _this12.image);

                for (key in _this12.editedItem) {
                  bodyFormData.append(key, _this12.editedItem[key]);
                }

                axios({
                  method: 'post',
                  url: route('products.store'),
                  data: bodyFormData,
                  headers: {
                    'Content-Type': 'multipart/form-data'
                  }
                }).then(function (response) {
                  console.log(response.data.message);

                  _this12.openSnack(response.data.message, "success");

                  _this12.getProducts();
                })["catch"](function (error) {
                  _this12.openSnack("ha ocurrido un error al momento de crear el producto, por favor comuniquese con el administrador del sistema.", "error");

                  console.log(error);
                });

              case 4:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    },
    updateProduct: function updateProduct() {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee9() {
        var bodyFormData, key;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                bodyFormData = new FormData();
                bodyFormData.append('image', _this13.image);

                for (key in _this13.editedItem) {
                  bodyFormData.append(key, _this13.editedItem[key]);
                }

                bodyFormData.append('_method', 'PUT');
                bodyFormData.append('changeExistImage', _this13.changeExistImage);
                axios({
                  method: 'post',
                  url: route('products.update', _this13.editedItem.id),
                  data: bodyFormData,
                  headers: {
                    'Content-Type': 'multipart/form-data'
                  }
                }).then(function (response) {
                  console.log(response.data.message);

                  _this13.openSnack(response.data.message, "success");

                  _this13.getProducts();
                })["catch"](function (error) {
                  _this13.openSnack("ha ocurrido un error al momento de actualizar el producto, por favor comuniquese con el administrador del sistema.", "error");

                  console.log(error);
                });
                /*axios.put(route('products.update',this.editedItem.id),this.editedItem)
                    .then( response => {
                        this.openSnack(response.data.message,"success")
                        this.getProducts()
                    })
                    .catch(error =>{
                        this.openSnack("ha ocurrido un error al momento de actualizar el producto, por favor comuniquese con el administrador del sistema.","error")
                        console.log(error)
                    })*/

              case 6:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }))();
    },
    deleteProduct: function deleteProduct() {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee10() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                axios["delete"](route('products.destroy', _this14.editedItem.id)).then(function (response) {
                  _this14.openSnack(response.data.message, "success");

                  _this14.getProducts();
                })["catch"](function (error) {
                  _this14.openSnack("ha ocurrido un error al momento de eliminar el producto, por favor comuniquese con el administrador del sistema.", "error");

                  console.log(error);
                });

              case 1:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-croppa/dist/vue-croppa.css":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-croppa/dist/vue-croppa.css ***!
  \*************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".sk-fading-circle {\n  position: absolute; }\n  .sk-fading-circle .sk-circle {\n    width: 100%;\n    height: 100%;\n    position: absolute;\n    left: 0;\n    top: 0; }\n  .sk-fading-circle .sk-circle .sk-circle-indicator {\n    display: block;\n    margin: 0 auto;\n    width: 15%;\n    height: 15%;\n    border-radius: 100%;\n    -webkit-animation: sk-circleFadeDelay 1s infinite ease-in-out both;\n            animation: sk-circleFadeDelay 1s infinite ease-in-out both; }\n  .sk-fading-circle .sk-circle2 {\n    transform: rotate(30deg); }\n  .sk-fading-circle .sk-circle3 {\n    transform: rotate(60deg); }\n  .sk-fading-circle .sk-circle4 {\n    transform: rotate(90deg); }\n  .sk-fading-circle .sk-circle5 {\n    transform: rotate(120deg); }\n  .sk-fading-circle .sk-circle6 {\n    transform: rotate(150deg); }\n  .sk-fading-circle .sk-circle7 {\n    transform: rotate(180deg); }\n  .sk-fading-circle .sk-circle8 {\n    transform: rotate(210deg); }\n  .sk-fading-circle .sk-circle9 {\n    transform: rotate(240deg); }\n  .sk-fading-circle .sk-circle10 {\n    transform: rotate(270deg); }\n  .sk-fading-circle .sk-circle11 {\n    transform: rotate(300deg); }\n  .sk-fading-circle .sk-circle12 {\n    transform: rotate(330deg); }\n  .sk-fading-circle .sk-circle2 .sk-circle-indicator {\n    -webkit-animation-delay: -0.91667s;\n            animation-delay: -0.91667s; }\n  .sk-fading-circle .sk-circle3 .sk-circle-indicator {\n    -webkit-animation-delay: -0.83333s;\n            animation-delay: -0.83333s; }\n  .sk-fading-circle .sk-circle4 .sk-circle-indicator {\n    -webkit-animation-delay: -0.75s;\n            animation-delay: -0.75s; }\n  .sk-fading-circle .sk-circle5 .sk-circle-indicator {\n    -webkit-animation-delay: -0.66667s;\n            animation-delay: -0.66667s; }\n  .sk-fading-circle .sk-circle6 .sk-circle-indicator {\n    -webkit-animation-delay: -0.58333s;\n            animation-delay: -0.58333s; }\n  .sk-fading-circle .sk-circle7 .sk-circle-indicator {\n    -webkit-animation-delay: -0.5s;\n            animation-delay: -0.5s; }\n  .sk-fading-circle .sk-circle8 .sk-circle-indicator {\n    -webkit-animation-delay: -0.41667s;\n            animation-delay: -0.41667s; }\n  .sk-fading-circle .sk-circle9 .sk-circle-indicator {\n    -webkit-animation-delay: -0.33333s;\n            animation-delay: -0.33333s; }\n  .sk-fading-circle .sk-circle10 .sk-circle-indicator {\n    -webkit-animation-delay: -0.25s;\n            animation-delay: -0.25s; }\n  .sk-fading-circle .sk-circle11 .sk-circle-indicator {\n    -webkit-animation-delay: -0.16667s;\n            animation-delay: -0.16667s; }\n  .sk-fading-circle .sk-circle12 .sk-circle-indicator {\n    -webkit-animation-delay: -0.08333s;\n            animation-delay: -0.08333s; }\n\n@-webkit-keyframes sk-circleFadeDelay {\n  0%,\n  39%,\n  100% {\n    opacity: 0; }\n  40% {\n    opacity: 1; } }\n\n@keyframes sk-circleFadeDelay {\n  0%,\n  39%,\n  100% {\n    opacity: 0; }\n  40% {\n    opacity: 1; } }\n\n.croppa-container {\n  display: inline-block;\n  cursor: pointer;\n  transition: all 0.3s;\n  position: relative;\n  font-size: 0;\n  align-self: flex-start;\n  background-color: #e6e6e6;\n}\n.croppa-container canvas {\n  transition: all 0.3s;\n}\n.croppa-container:hover {\n  opacity: 0.7;\n}\n.croppa-container.croppa--dropzone {\n  box-shadow: inset 0 0 10px #333;\n}\n.croppa-container.croppa--dropzone canvas {\n  opacity: 0.5;\n}\n.croppa-container.croppa--disabled-cc {\n  cursor: default;\n}\n.croppa-container.croppa--disabled-cc:hover {\n  opacity: 1;\n}\n.croppa-container.croppa--has-target {\n  cursor: move;\n}\n.croppa-container.croppa--has-target:hover {\n  opacity: 1;\n}\n.croppa-container.croppa--has-target.croppa--disabled-mz {\n  cursor: default;\n}\n.croppa-container.croppa--disabled {\n  cursor: not-allowed;\n}\n.croppa-container.croppa--disabled:hover {\n  opacity: 1;\n}\n.croppa-container.croppa--passive {\n  cursor: default;\n}\n.croppa-container.croppa--passive:hover {\n  opacity: 1;\n}\n.croppa-container svg.icon-remove {\n  position: absolute;\n  background: #fff;\n  border-radius: 50%;\n  filter: drop-shadow(-2px 2px 2px rgba(0,0,0,0.7));\n  z-index: 10;\n  cursor: pointer;\n  border: 2px solid #fff;\n}\n\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.croppa-container[data-v-616aa9dc]{\n    max-height: 200px;\n}\n.fade-enter-active[data-v-616aa9dc], .fade-leave-active[data-v-616aa9dc] {\n    transition: opacity .1s;\n}\n.fade-enter[data-v-616aa9dc], .fade-leave-to[data-v-616aa9dc] /* .fade-leave-active below version 2.1.8 */ {\n    opacity: 0;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/***/ ((module) => {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : 0
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./node_modules/vue-croppa/dist/vue-croppa.css":
/*!*****************************************************!*\
  !*** ./node_modules/vue-croppa/dist/vue-croppa.css ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_vue_croppa_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./vue-croppa.css */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-croppa/dist/vue-croppa.css");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_vue_croppa_css__WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_vue_croppa_css__WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_id_616aa9dc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_id_616aa9dc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_id_616aa9dc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./resources/js/Pages/Private/Products/Index.vue":
/*!*******************************************************!*\
  !*** ./resources/js/Pages/Private/Products/Index.vue ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Index_vue_vue_type_template_id_616aa9dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=616aa9dc&scoped=true& */ "./resources/js/Pages/Private/Products/Index.vue?vue&type=template&id=616aa9dc&scoped=true&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Private/Products/Index.vue?vue&type=script&lang=js&");
/* harmony import */ var _Index_vue_vue_type_style_index_0_id_616aa9dc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css& */ "./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Index_vue_vue_type_template_id_616aa9dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Index_vue_vue_type_template_id_616aa9dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "616aa9dc",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Private/Products/Index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Private/Products/Index.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/Pages/Private/Products/Index.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css& ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_id_616aa9dc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=style&index=0&id=616aa9dc&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/Pages/Private/Products/Index.vue?vue&type=template&id=616aa9dc&scoped=true&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/Pages/Private/Products/Index.vue?vue&type=template&id=616aa9dc&scoped=true& ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_616aa9dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_616aa9dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_616aa9dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Index.vue?vue&type=template&id=616aa9dc&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=template&id=616aa9dc&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=template&id=616aa9dc&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Private/Products/Index.vue?vue&type=template&id=616aa9dc&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-container",
        { attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { staticClass: "d-inline-flex", attrs: { sm: "12", md: "12" } },
                [
                  _c(
                    "v-row",
                    [
                      _c("v-col", { attrs: { cols: "6", sm: "6", md: "6" } }, [
                        _c("h3", [_vm._v("Catalogo de productos")])
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          attrs: { align: "end", cols: "6", sm: "6", md: "6" }
                        },
                        [
                          _c(
                            "v-dialog",
                            {
                              attrs: { "max-width": "900px", persistent: "" },
                              scopedSlots: _vm._u([
                                {
                                  key: "activator",
                                  fn: function(ref) {
                                    var on = ref.on
                                    var attrs = ref.attrs
                                    return [
                                      _vm.$page.props.auth.user.permission.includes(
                                        "products.new"
                                      )
                                        ? _c(
                                            "v-btn",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass:
                                                    "mb-2 float-right",
                                                  attrs: {
                                                    color: "primary",
                                                    dark: ""
                                                  }
                                                },
                                                "v-btn",
                                                attrs,
                                                false
                                              ),
                                              on
                                            ),
                                            [
                                              _vm._v(
                                                "\n                                    Nuevo\n                                "
                                              )
                                            ]
                                          )
                                        : _vm._e()
                                    ]
                                  }
                                }
                              ]),
                              model: {
                                value: _vm.dialog,
                                callback: function($$v) {
                                  _vm.dialog = $$v
                                },
                                expression: "dialog"
                              }
                            },
                            [
                              _vm._v(" "),
                              _c(
                                "v-card",
                                [
                                  _c("v-card-title", [
                                    _c("span", { staticClass: "text-h5" }, [
                                      _vm._v(_vm._s(_vm.formTitle))
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "v-card-text",
                                    [
                                      _c(
                                        "v-form",
                                        {
                                          ref: "formModal",
                                          attrs: { "lazy-validation": "" }
                                        },
                                        [
                                          _c(
                                            "v-container",
                                            [
                                              _c(
                                                "v-row",
                                                { attrs: { "no-gutters": "" } },
                                                [
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "8",
                                                        "align-self": "center"
                                                      }
                                                    },
                                                    [
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          filled: "",
                                                          "background-color":
                                                            "grey lighten-5",
                                                          disabled:
                                                            _vm.viewOnly,
                                                          readonly: "",
                                                          label: "Codigo",
                                                          hint:
                                                            "se genera automaticamente",
                                                          "persistent-hint": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.editedItem
                                                              .internalCode,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.editedItem,
                                                              "internalCode",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "editedItem.internalCode"
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          disabled:
                                                            _vm.viewOnly,
                                                          label: "Nombre",
                                                          rules: [
                                                            _vm.rules.required
                                                          ]
                                                        },
                                                        on: {
                                                          change:
                                                            _vm.generateCode
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.editedItem.name,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.editedItem,
                                                              "name",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "editedItem.name"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      staticClass:
                                                        "justify-center",
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "3",
                                                        "offset-md": "1",
                                                        "align-self": "center"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          directives: [
                                                            {
                                                              name: "show",
                                                              rawName: "v-show",
                                                              value: !_vm.changingImage,
                                                              expression:
                                                                "!changingImage"
                                                            }
                                                          ],
                                                          staticClass:
                                                            "justify-center"
                                                        },
                                                        [
                                                          _c(
                                                            "div",
                                                            {
                                                              staticClass:
                                                                "pl-md-3"
                                                            },
                                                            [
                                                              _c("v-img", {
                                                                attrs: {
                                                                  "max-width":
                                                                    "184.5",
                                                                  "max-height":
                                                                    "184.5",
                                                                  src: this.url
                                                                }
                                                              })
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "div",
                                                            {
                                                              staticClass:
                                                                "pt-3 justify-center"
                                                            },
                                                            [
                                                              !_vm.editedItem.id
                                                                ? _c(
                                                                    "v-btn",
                                                                    {
                                                                      attrs: {
                                                                        small:
                                                                          "",
                                                                        block:
                                                                          "",
                                                                        text:
                                                                          "",
                                                                        color:
                                                                          "primary",
                                                                        align:
                                                                          "center"
                                                                      },
                                                                      on: {
                                                                        click: function(
                                                                          $event
                                                                        ) {
                                                                          _vm.changingImage = true
                                                                        }
                                                                      }
                                                                    },
                                                                    [
                                                                      _vm._v(
                                                                        "\n                                                                    seleccionar imagen\n                                                                "
                                                                      )
                                                                    ]
                                                                  )
                                                                : _c(
                                                                    "v-btn",
                                                                    {
                                                                      attrs: {
                                                                        small:
                                                                          "",
                                                                        block:
                                                                          "",
                                                                        disabled:
                                                                          _vm.viewOnly,
                                                                        text:
                                                                          "",
                                                                        color:
                                                                          "primary",
                                                                        align:
                                                                          "center"
                                                                      },
                                                                      on: {
                                                                        click:
                                                                          _vm.handleChangeExistedImage
                                                                      }
                                                                    },
                                                                    [
                                                                      _vm._v(
                                                                        "\n                                                                    cambiar imagen\n                                                                "
                                                                      )
                                                                    ]
                                                                  )
                                                            ],
                                                            1
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          directives: [
                                                            {
                                                              name: "show",
                                                              rawName: "v-show",
                                                              value:
                                                                _vm.changingImage,
                                                              expression:
                                                                "changingImage"
                                                            }
                                                          ],
                                                          staticClass:
                                                            "justify-center"
                                                        },
                                                        [
                                                          _c("croppa", {
                                                            staticClass: "pb-4",
                                                            attrs: {
                                                              width: 200,
                                                              height: 200,
                                                              placeholder:
                                                                "Toca para aquí para buscar.",
                                                              "placeholder-font-size": 14,
                                                              accept: "image/*",
                                                              "prevent-white-space":
                                                                "",
                                                              "remove-button-color":
                                                                "black"
                                                            },
                                                            model: {
                                                              value:
                                                                _vm.myCroppa,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.myCroppa = $$v
                                                              },
                                                              expression:
                                                                "myCroppa"
                                                            }
                                                          }),
                                                          _vm._v(" "),
                                                          _c(
                                                            "div",
                                                            {
                                                              staticClass:
                                                                "pt-3 justify-center"
                                                            },
                                                            [
                                                              _c(
                                                                "v-row",
                                                                [
                                                                  _c(
                                                                    "v-col",
                                                                    {
                                                                      attrs: {
                                                                        cols:
                                                                          "6",
                                                                        sm: "6",
                                                                        md: "6"
                                                                      }
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "v-btn",
                                                                        {
                                                                          attrs: {
                                                                            small:
                                                                              "",
                                                                            block:
                                                                              "",
                                                                            text:
                                                                              "",
                                                                            color:
                                                                              "primary",
                                                                            align:
                                                                              "center"
                                                                          },
                                                                          on: {
                                                                            click:
                                                                              _vm.getImageResize
                                                                          }
                                                                        },
                                                                        [
                                                                          _vm
                                                                            .editedItem
                                                                            .id
                                                                            ? _c(
                                                                                "span",
                                                                                [
                                                                                  _vm._v(
                                                                                    "aceptar"
                                                                                  )
                                                                                ]
                                                                              )
                                                                            : _c(
                                                                                "span",
                                                                                [
                                                                                  _vm._v(
                                                                                    "Guardar"
                                                                                  )
                                                                                ]
                                                                              )
                                                                        ]
                                                                      )
                                                                    ],
                                                                    1
                                                                  ),
                                                                  _vm._v(" "),
                                                                  _c(
                                                                    "v-col",
                                                                    {
                                                                      attrs: {
                                                                        cols:
                                                                          "6",
                                                                        sm: "6",
                                                                        md: "6"
                                                                      }
                                                                    },
                                                                    [
                                                                      !_vm
                                                                        .editedItem
                                                                        .id
                                                                        ? _c(
                                                                            "v-btn",
                                                                            {
                                                                              attrs: {
                                                                                small:
                                                                                  "",
                                                                                block:
                                                                                  "",
                                                                                text:
                                                                                  "",
                                                                                color:
                                                                                  "grey",
                                                                                align:
                                                                                  "center"
                                                                              },
                                                                              on: {
                                                                                click: function(
                                                                                  $event
                                                                                ) {
                                                                                  _vm.changingImage = false
                                                                                }
                                                                              }
                                                                            },
                                                                            [
                                                                              _vm._v(
                                                                                "\n                                                                            cancelar\n                                                                        "
                                                                              )
                                                                            ]
                                                                          )
                                                                        : _c(
                                                                            "v-btn",
                                                                            {
                                                                              attrs: {
                                                                                small:
                                                                                  "",
                                                                                block:
                                                                                  "",
                                                                                text:
                                                                                  "",
                                                                                color:
                                                                                  "grey",
                                                                                align:
                                                                                  "center"
                                                                              },
                                                                              on: {
                                                                                click:
                                                                                  _vm.handleCancelChangeExistedImage
                                                                              }
                                                                            },
                                                                            [
                                                                              _vm._v(
                                                                                "\n                                                                            anterior\n                                                                        "
                                                                              )
                                                                            ]
                                                                          )
                                                                    ],
                                                                    1
                                                                  )
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ]
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-row",
                                                [
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "12"
                                                      }
                                                    },
                                                    [
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          disabled:
                                                            _vm.viewOnly,
                                                          label: "Descripción"
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.editedItem
                                                              .description,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.editedItem,
                                                              "description",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "editedItem.description"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-row",
                                                        {
                                                          attrs: {
                                                            "no-gutters": ""
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "9",
                                                                sm: "9",
                                                                md: "9"
                                                              }
                                                            },
                                                            [
                                                              _c("v-select", {
                                                                attrs: {
                                                                  disabled:
                                                                    _vm.viewOnly,
                                                                  items:
                                                                    _vm.brands,
                                                                  "item-text":
                                                                    "name",
                                                                  "item-value":
                                                                    "id",
                                                                  label:
                                                                    "Marca",
                                                                  hint:
                                                                    "selecciona la marca para este producto.",
                                                                  rules: [
                                                                    _vm.rules
                                                                      .required
                                                                  ]
                                                                },
                                                                on: {
                                                                  change:
                                                                    _vm.generateCode
                                                                },
                                                                model: {
                                                                  value:
                                                                    _vm
                                                                      .editedItem
                                                                      .productBrand_id,
                                                                  callback: function(
                                                                    $$v
                                                                  ) {
                                                                    _vm.$set(
                                                                      _vm.editedItem,
                                                                      "productBrand_id",
                                                                      $$v
                                                                    )
                                                                  },
                                                                  expression:
                                                                    "editedItem.productBrand_id"
                                                                }
                                                              })
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              staticClass:
                                                                "justify-start",
                                                              attrs: {
                                                                cols: "3",
                                                                sm: "3",
                                                                md: "3",
                                                                "align-self":
                                                                  "center"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-btn",
                                                                {
                                                                  attrs: {
                                                                    plain: "",
                                                                    small: "",
                                                                    dark: "",
                                                                    color:
                                                                      "grey lighten-1"
                                                                  },
                                                                  on: {
                                                                    click: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.openHybridDialog(
                                                                        "brand"
                                                                      )
                                                                    }
                                                                  }
                                                                },
                                                                [
                                                                  _c("v-icon", [
                                                                    _vm._v(
                                                                      "mdi-plus"
                                                                    )
                                                                  ])
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-row",
                                                        {
                                                          attrs: {
                                                            "no-gutters": ""
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "9",
                                                                sm: "9",
                                                                md: "9"
                                                              }
                                                            },
                                                            [
                                                              _c("v-select", {
                                                                attrs: {
                                                                  clearable: "",
                                                                  disabled:
                                                                    _vm.viewOnly,
                                                                  items:
                                                                    _vm.categories,
                                                                  "item-text":
                                                                    "name",
                                                                  "item-value":
                                                                    "id",
                                                                  label:
                                                                    "Categoria",
                                                                  hint:
                                                                    "selecciona a que categoria pertenece este producto.",
                                                                  rules: [
                                                                    _vm.rules
                                                                      .required
                                                                  ]
                                                                },
                                                                model: {
                                                                  value:
                                                                    _vm
                                                                      .editedItem
                                                                      .productCategory_id,
                                                                  callback: function(
                                                                    $$v
                                                                  ) {
                                                                    _vm.$set(
                                                                      _vm.editedItem,
                                                                      "productCategory_id",
                                                                      $$v
                                                                    )
                                                                  },
                                                                  expression:
                                                                    "editedItem.productCategory_id"
                                                                }
                                                              })
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              staticClass:
                                                                "justify-start",
                                                              attrs: {
                                                                cols: "3",
                                                                sm: "3",
                                                                md: "3",
                                                                "align-self":
                                                                  "center"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-btn",
                                                                {
                                                                  attrs: {
                                                                    plain: "",
                                                                    small: "",
                                                                    dark: "",
                                                                    color:
                                                                      "grey lighten-1"
                                                                  },
                                                                  on: {
                                                                    click: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.openHybridDialog(
                                                                        "category"
                                                                      )
                                                                    }
                                                                  }
                                                                },
                                                                [
                                                                  _c("v-icon", [
                                                                    _vm._v(
                                                                      "mdi-plus"
                                                                    )
                                                                  ])
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-row",
                                                        {
                                                          attrs: {
                                                            "no-gutters": ""
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "9",
                                                                sm: "9",
                                                                md: "9"
                                                              }
                                                            },
                                                            [
                                                              _c("v-select", {
                                                                attrs: {
                                                                  disabled:
                                                                    _vm.viewOnly,
                                                                  items:
                                                                    _vm.groups,
                                                                  "item-text":
                                                                    "name",
                                                                  "item-value":
                                                                    "id",
                                                                  label:
                                                                    "Grupo",
                                                                  hint:
                                                                    "selecciona a que grupo pertenece este producto.",
                                                                  rules: [
                                                                    _vm.rules
                                                                      .required
                                                                  ]
                                                                },
                                                                model: {
                                                                  value:
                                                                    _vm
                                                                      .editedItem
                                                                      .productGroup_id,
                                                                  callback: function(
                                                                    $$v
                                                                  ) {
                                                                    _vm.$set(
                                                                      _vm.editedItem,
                                                                      "productGroup_id",
                                                                      $$v
                                                                    )
                                                                  },
                                                                  expression:
                                                                    "editedItem.productGroup_id"
                                                                }
                                                              })
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              staticClass:
                                                                "justify-start",
                                                              attrs: {
                                                                cols: "3",
                                                                sm: "3",
                                                                md: "3",
                                                                "align-self":
                                                                  "center"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-btn",
                                                                {
                                                                  attrs: {
                                                                    plain: "",
                                                                    small: "",
                                                                    dark: "",
                                                                    color:
                                                                      "grey lighten-1"
                                                                  },
                                                                  on: {
                                                                    click: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.openHybridDialog(
                                                                        "group"
                                                                      )
                                                                    }
                                                                  }
                                                                },
                                                                [
                                                                  _c("v-icon", [
                                                                    _vm._v(
                                                                      "mdi-plus"
                                                                    )
                                                                  ])
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-row",
                                                        {
                                                          attrs: {
                                                            "no-gutters": ""
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "9",
                                                                sm: "9",
                                                                md: "9"
                                                              }
                                                            },
                                                            [
                                                              _c("v-select", {
                                                                attrs: {
                                                                  disabled:
                                                                    _vm.viewOnly,
                                                                  items:
                                                                    _vm.measurementUnits,
                                                                  "item-text":
                                                                    "name",
                                                                  "item-value":
                                                                    "id",
                                                                  label:
                                                                    "Unidad de medida",
                                                                  hint:
                                                                    "selecciona la unidad de medida para este producto.",
                                                                  rules: [
                                                                    _vm.rules
                                                                      .required
                                                                  ]
                                                                },
                                                                model: {
                                                                  value:
                                                                    _vm
                                                                      .editedItem
                                                                      .measurementUnit_id,
                                                                  callback: function(
                                                                    $$v
                                                                  ) {
                                                                    _vm.$set(
                                                                      _vm.editedItem,
                                                                      "measurementUnit_id",
                                                                      $$v
                                                                    )
                                                                  },
                                                                  expression:
                                                                    "editedItem.measurementUnit_id"
                                                                }
                                                              })
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              staticClass:
                                                                "justify-start",
                                                              attrs: {
                                                                cols: "3",
                                                                sm: "3",
                                                                md: "3",
                                                                "align-self":
                                                                  "center"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-btn",
                                                                {
                                                                  attrs: {
                                                                    plain: "",
                                                                    small: "",
                                                                    dark: "",
                                                                    color:
                                                                      "grey lighten-1"
                                                                  },
                                                                  on: {
                                                                    click: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.openHybridDialog(
                                                                        "measurementUnit"
                                                                      )
                                                                    }
                                                                  }
                                                                },
                                                                [
                                                                  _c("v-icon", [
                                                                    _vm._v(
                                                                      "mdi-plus"
                                                                    )
                                                                  ])
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          disabled:
                                                            _vm.viewOnly,
                                                          label:
                                                            "Cantidad minima en inventario",
                                                          rules: [
                                                            _vm.rules.required,
                                                            _vm.rules.number
                                                          ],
                                                          hint:
                                                            "Se mostrará una alerta al llegar a esta cantidad"
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.editedItem
                                                              .quantityAlert,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.editedItem,
                                                              "quantityAlert",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "editedItem.quantityAlert"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c("v-switch", {
                                                        attrs: {
                                                          disabled:
                                                            _vm.viewOnly,
                                                          label:
                                                            "Aplica para descuento"
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.editedItem
                                                              .discountEnabled,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.editedItem,
                                                              "discountEnabled",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "editedItem.discountEnabled"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _vm.$page.props.auth.user.permission.includes(
                                                    "private.dashboard.admin"
                                                  )
                                                    ? _c(
                                                        "v-col",
                                                        {
                                                          attrs: {
                                                            cols: "12",
                                                            sm: "12",
                                                            md: "4"
                                                          }
                                                        },
                                                        [
                                                          _c("v-text-field", {
                                                            attrs: {
                                                              disabled:
                                                                _vm.viewOnly,
                                                              label:
                                                                "Precio de compra",
                                                              rules: [
                                                                _vm.rules.price
                                                              ],
                                                              hint:
                                                                "formato: 00.00"
                                                            },
                                                            on: {
                                                              change:
                                                                _vm.calculateEconomicGains
                                                            },
                                                            model: {
                                                              value:
                                                                _vm.editedItem
                                                                  .purchasePrice,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.editedItem,
                                                                  "purchasePrice",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "editedItem.purchasePrice"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "4"
                                                      }
                                                    },
                                                    [
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          disabled:
                                                            _vm.viewOnly,
                                                          label:
                                                            "Precio de venta",
                                                          rules: [
                                                            _vm.rules.required,
                                                            _vm.rules.price
                                                          ],
                                                          hint: "formato: 00.00"
                                                        },
                                                        on: {
                                                          change:
                                                            _vm.calculateEconomicGains
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.editedItem
                                                              .salePrice,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.editedItem,
                                                              "salePrice",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "editedItem.salePrice"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _vm.$page.props.auth.user.permission.includes(
                                                    "private.dashboard.admin"
                                                  )
                                                    ? _c(
                                                        "v-col",
                                                        {
                                                          attrs: {
                                                            cols: "12",
                                                            sm: "12",
                                                            md: "4"
                                                          }
                                                        },
                                                        [
                                                          _vm.$page.props.auth.user.permission.includes(
                                                            "private.dashboard.admin"
                                                          )
                                                            ? _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    disabled:
                                                                      _vm.viewOnly,
                                                                    readonly:
                                                                      "",
                                                                    label:
                                                                      "Ganancia"
                                                                  },
                                                                  model: {
                                                                    value:
                                                                      _vm
                                                                        .editedItem
                                                                        .economicGains,
                                                                    callback: function(
                                                                      $$v
                                                                    ) {
                                                                      _vm.$set(
                                                                        _vm.editedItem,
                                                                        "economicGains",
                                                                        $$v
                                                                      )
                                                                    },
                                                                    expression:
                                                                      "editedItem.economicGains"
                                                                  }
                                                                }
                                                              )
                                                            : _vm._e()
                                                        ],
                                                        1
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: "6"
                                                      }
                                                    },
                                                    [
                                                      !_vm.editedItem.id
                                                        ? _c("v-switch", {
                                                            attrs: {
                                                              label:
                                                                "¿Cargar cantidad inicial al inventario?"
                                                            },
                                                            model: {
                                                              value:
                                                                _vm.editedItem
                                                                  .booleanInitalQuantity,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.editedItem,
                                                                  "booleanInitalQuantity",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "editedItem.booleanInitalQuantity"
                                                            }
                                                          })
                                                        : _vm._e()
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-col",
                                                    {
                                                      attrs: {
                                                        cols: "12",
                                                        sm: "12",
                                                        md: _vm.$page.props.auth.user.permission.includes(
                                                          "private.dashboard.admin"
                                                        )
                                                          ? 6
                                                          : 4
                                                      }
                                                    },
                                                    [
                                                      !_vm.editedItem.id &&
                                                      _vm.editedItem
                                                        .booleanInitalQuantity ===
                                                        true
                                                        ? _c("v-text-field", {
                                                            attrs: {
                                                              disabled:
                                                                _vm.viewOnly,
                                                              label:
                                                                "Cantidad inicial",
                                                              rules: [
                                                                _vm.rules.number
                                                              ]
                                                            },
                                                            model: {
                                                              value:
                                                                _vm.editedItem
                                                                  .initialQuantity,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.editedItem,
                                                                  "initialQuantity",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "editedItem.initialQuantity"
                                                            }
                                                          })
                                                        : _vm._e()
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-card-actions",
                                    [
                                      _c("v-spacer"),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: {
                                            color: "secondary",
                                            text: ""
                                          },
                                          on: {
                                            click: function($event) {
                                              return _vm.closeDialog(
                                                "dialogEdit"
                                              )
                                            }
                                          }
                                        },
                                        [
                                          !_vm.viewOnly
                                            ? _c("span", [_vm._v("Cancelar")])
                                            : _c("span", [_vm._v("Cerrar")])
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value: !_vm.viewOnly,
                                              expression: "!viewOnly"
                                            }
                                          ],
                                          attrs: { color: "primary", dark: "" },
                                          on: { click: _vm.save }
                                        },
                                        [
                                          _vm._v(
                                            "\n                                        Guardar\n                                    "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _vm.$page.props.auth.user.permission.includes(
                            "products.destroy"
                          )
                            ? _c(
                                "v-dialog",
                                {
                                  attrs: {
                                    "max-width": "500px",
                                    persistent: ""
                                  },
                                  model: {
                                    value: _vm.dialogDelete,
                                    callback: function($$v) {
                                      _vm.dialogDelete = $$v
                                    },
                                    expression: "dialogDelete"
                                  }
                                },
                                [
                                  _c(
                                    "v-card",
                                    [
                                      _c(
                                        "v-list-item",
                                        { staticClass: "p-0" },
                                        [
                                          _c(
                                            "v-list-item-content",
                                            {
                                              staticClass:
                                                "pt-8 d-flex justify-center",
                                              attrs: { align: "center" }
                                            },
                                            [
                                              _c("v-img", {
                                                attrs: {
                                                  "max-height": "60",
                                                  "max-width": "60",
                                                  src:
                                                    "/images/icons/precaucion.png",
                                                  position: "center center"
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c("p", { staticClass: "pt-3" }, [
                                                _vm._v(
                                                  "¿" +
                                                    _vm._s(
                                                      _vm.$page.props.auth.user
                                                        .name
                                                    ) +
                                                    ", Realmente deseas eliminar este grupo de producto?"
                                                )
                                              ])
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-card-actions",
                                        [
                                          _c("v-spacer"),
                                          _vm._v(" "),
                                          _c(
                                            "v-btn",
                                            {
                                              attrs: {
                                                color: "secondary",
                                                text: ""
                                              },
                                              on: {
                                                click: function($event) {
                                                  return _vm.closeDialog(
                                                    "dialogDelete"
                                                  )
                                                }
                                              }
                                            },
                                            [_vm._v("Cancelar")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-btn",
                                            {
                                              attrs: {
                                                color: "primary",
                                                text: ""
                                              },
                                              on: {
                                                click: _vm.deleteItemConfirm
                                              }
                                            },
                                            [_vm._v("Si, Eliminar")]
                                          ),
                                          _vm._v(" "),
                                          _c("v-spacer")
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { sm: "12", md: "12" } },
                [
                  _c(
                    "v-card",
                    [
                      _c("v-data-table", {
                        staticClass: "elevation-1 p-md-5",
                        attrs: {
                          headers: _vm.headers,
                          items: _vm.products,
                          search: _vm.search,
                          loading: _vm.loadTable,
                          "items-per-page": 5,
                          "loading-text": "Un momento por favor...",
                          "no-results-text":
                            "Lo sentimos no se ha encontrado una grupo de producto con esos datos de busqueda.",
                          "header-props": { "sort-by-text": "Ordenar por" },
                          "footer-props": {
                            "items-per-page-text": "productos por página",
                            "items-per-page-all-text": "Todos"
                          }
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "top",
                            fn: function() {
                              return [
                                _c(
                                  "v-toolbar",
                                  {
                                    staticClass: "pb-15",
                                    attrs: { flat: "", height: "110" }
                                  },
                                  [
                                    _c(
                                      "v-row",
                                      [
                                        _c(
                                          "v-col",
                                          {
                                            attrs: {
                                              cols: "12",
                                              sm: "12",
                                              md: "6",
                                              "offset-md": "3",
                                              order: "first",
                                              "order-sm": "last"
                                            }
                                          },
                                          [
                                            _c("v-text-field", {
                                              attrs: {
                                                align: "center",
                                                "append-icon": "mdi-magnify",
                                                label:
                                                  "Busca aquí según las columnas en la tabla.",
                                                "single-line": "",
                                                "hide-details": ""
                                              },
                                              model: {
                                                value: _vm.search,
                                                callback: function($$v) {
                                                  _vm.search = $$v
                                                },
                                                expression: "search"
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ]
                            },
                            proxy: true
                          },
                          {
                            key: "item.discountEnabled",
                            fn: function(ref) {
                              var item = ref.item
                              return [
                                _c(
                                  "v-chip",
                                  {
                                    attrs: {
                                      small: "",
                                      color:
                                        item.discountEnabled === true
                                          ? "pink lighten-4"
                                          : "grey lighten-4"
                                    }
                                  },
                                  [
                                    item.discountEnabled === true
                                      ? _c("span", [_vm._v("Habilitado")])
                                      : _c("span", [_vm._v("Deshabilitado")])
                                  ]
                                )
                              ]
                            }
                          },
                          {
                            key: "item.actions",
                            fn: function(ref) {
                              var item = ref.item
                              return [
                                _vm.$page.props.auth.user.permission.includes(
                                  "products.show"
                                )
                                  ? _c(
                                      "v-icon",
                                      {
                                        attrs: { small: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.assignItem(
                                              item,
                                              "dialogEdit",
                                              "show"
                                            )
                                          }
                                        }
                                      },
                                      [_vm._v("mdi-eye")]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.$page.props.auth.user.permission.includes(
                                  "products.update"
                                )
                                  ? _c(
                                      "v-icon",
                                      {
                                        staticClass: "mr-2",
                                        attrs: { small: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.assignItem(
                                              item,
                                              "dialogEdit",
                                              "default"
                                            )
                                          }
                                        }
                                      },
                                      [_vm._v("mdi-pencil")]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.$page.props.auth.user.permission.includes(
                                  "products.destroy"
                                )
                                  ? _c(
                                      "v-icon",
                                      {
                                        attrs: { small: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.assignItem(
                                              item,
                                              "dialogDelete",
                                              "default"
                                            )
                                          }
                                        }
                                      },
                                      [_vm._v("mdi-delete")]
                                    )
                                  : _vm._e()
                              ]
                            }
                          },
                          {
                            key: "no-data",
                            fn: function() {
                              return [
                                _c(
                                  "v-list-item",
                                  { staticClass: "p-0" },
                                  [
                                    _c(
                                      "v-list-item-content",
                                      {
                                        staticClass:
                                          "py-12 d-flex justify-center",
                                        attrs: { align: "center" }
                                      },
                                      [
                                        _c("v-img", {
                                          attrs: {
                                            "max-height": "60",
                                            "max-width": "60",
                                            src: "/images/icons/feliz.png",
                                            position: "center center"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("p", { staticClass: "pt-3" }, [
                                          _vm._v(
                                            _vm._s(
                                              _vm.$page.props.auth.user.name
                                            ) +
                                              ", Parece que no tienes ningún producto agregado al sistema, puedes agregar uno haciendo clic "
                                          ),
                                          _c(
                                            "span",
                                            {
                                              staticClass: "primary--text",
                                              on: {
                                                click: function($event) {
                                                  _vm.dialog = true
                                                }
                                              }
                                            },
                                            [_c("strong", [_vm._v("aqui")])]
                                          ),
                                          _vm._v(".")
                                        ])
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ]
                            },
                            proxy: true
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c(
                        "v-snackbar",
                        {
                          attrs: {
                            color: _vm.snackbar_color,
                            right: "",
                            top: "",
                            timeout: _vm.timeout
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "action",
                              fn: function(ref) {
                                var attrs = ref.attrs
                                return [
                                  _c(
                                    "v-btn",
                                    _vm._b(
                                      {
                                        attrs: {
                                          color:
                                            _vm.snackbar_color == "error"
                                              ? "white"
                                              : "white",
                                          text: ""
                                        },
                                        on: {
                                          click: function($event) {
                                            _vm.snackbar = false
                                          }
                                        }
                                      },
                                      "v-btn",
                                      attrs,
                                      false
                                    ),
                                    [
                                      _vm._v(
                                        "\n                                cerrar\n                            "
                                      )
                                    ]
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.snackbar,
                            callback: function($$v) {
                              _vm.snackbar = $$v
                            },
                            expression: "snackbar"
                          }
                        },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(_vm.snackbar_text) +
                              "\n                        "
                          )
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-dialog",
            {
              attrs: { "max-width": "500px", persistent: "" },
              model: {
                value: _vm.hybridDialog,
                callback: function($$v) {
                  _vm.hybridDialog = $$v
                },
                expression: "hybridDialog"
              }
            },
            [
              _c(
                "v-card",
                [
                  _c("v-card-title", [
                    _c("span", { staticClass: "text-h5" }, [
                      _vm._v(_vm._s(_vm.formHybridTitle))
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card-text",
                    [
                      _c(
                        "v-form",
                        {
                          ref: "formHybridDialog",
                          attrs: { "lazy-validation": "" }
                        },
                        [
                          _c(
                            "v-container",
                            [
                              _c(
                                "v-row",
                                [
                                  _c(
                                    "v-col",
                                    {
                                      attrs: { cols: "12", sm: "12", md: "12" }
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Nombre",
                                          rules: [_vm.rules.required],
                                          hint:
                                            _vm.typeHybridDialog ==
                                            "measurementUnit"
                                              ? "nombres en plural para gestionar mejor el inventario (cajas, unidades, litros)"
                                              : ""
                                        },
                                        model: {
                                          value: _vm.defaultHybridtItem.name,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.defaultHybridtItem,
                                              "name",
                                              $$v
                                            )
                                          },
                                          expression: "defaultHybridtItem.name"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _vm.typeHybridDialog !== "measurementUnit"
                                    ? _c(
                                        "v-col",
                                        {
                                          attrs: {
                                            cols: "12",
                                            sm: "12",
                                            md: "12"
                                          }
                                        },
                                        [
                                          _c("v-text-field", {
                                            attrs: { label: "Descripción" },
                                            model: {
                                              value:
                                                _vm.defaultHybridtItem
                                                  .description,
                                              callback: function($$v) {
                                                _vm.$set(
                                                  _vm.defaultHybridtItem,
                                                  "description",
                                                  $$v
                                                )
                                              },
                                              expression:
                                                "defaultHybridtItem.description"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _vm.typeHybridDialog === "measurementUnit"
                                    ? _c(
                                        "v-col",
                                        {
                                          attrs: {
                                            cols: "12",
                                            sm: "12",
                                            md: "12"
                                          }
                                        },
                                        [
                                          _c("v-text-field", {
                                            attrs: { label: "Nombre corto" },
                                            model: {
                                              value:
                                                _vm.defaultHybridtItem
                                                  .shortName,
                                              callback: function($$v) {
                                                _vm.$set(
                                                  _vm.defaultHybridtItem,
                                                  "shortName",
                                                  $$v
                                                )
                                              },
                                              expression:
                                                "defaultHybridtItem.shortName"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    : _vm._e()
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "secondary", text: "" },
                          on: { click: _vm.closeHybridDialog }
                        },
                        [_c("span", [_vm._v("Cancelar")])]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "primary", dark: "" },
                          on: {
                            click: function($event) {
                              return _vm.saveHybridItem()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Guardar\n                    "
                          )
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);