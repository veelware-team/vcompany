require('./bootstrap');
import Vue from 'vue'
import { App, plugin } from '@inertiajs/inertia-vue'
import Vuelidate from 'vuelidate'
import vuetify from './plugins/vuetify' // path to vuetify export
import MultiFiltersPlugin from './plugins/MultiFilters'
import AppLayout from './Shared/Layouts/AppLayout'
import PublicLayout from './Shared/Layouts/PublicLayout'
import DefLayout from './Shared/Layouts/DefaultLayout'
import Croppa from 'vue-croppa'
import VueMeta from 'vue-meta'


Vue.mixin({ methods: { route: window.route } })
Vue.use(plugin)
Vue.use(Vuelidate)
Vue.use(Croppa)
Vue.use(VueMeta)
Vue.use(MultiFiltersPlugin);
const el = document.getElementById('app')
if(el){
    new Vue({
        vuetify,
        render: h => h(App, {
            props: {
                initialPage: JSON.parse(el.dataset.page),
                resolveComponent: name => import(`./Pages/${name}`)
                    .then(({ default: page }) => {
                        if (page.layout === undefined && name.startsWith('Public/Index')) {
                            page.layout = DefLayout
                        }
                        if (page.layout === undefined && name.startsWith('Public/')) {
                            page.layout = PublicLayout
                        }
                        if (page.layout === undefined && name.startsWith('Private/')) {
                            page.layout = AppLayout
                        }
                        return page
                    }),
            },
        }),
    }).$mount(el)
}
