import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/lib/util/colors'
import '@mdi/font/css/materialdesignicons.css'
Vue.use(Vuetify)

const opts = {
    theme: {
        icons: {
            iconfont: 'mdi',
        },
        themes: {
            light: {
                primary: colors.pink.lighten2,
                secondary: colors.grey.darken4,
                accent: '#82B1FF',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107',
            },
        },
    },
}

export default new Vuetify(opts)
