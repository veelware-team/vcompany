@extends('errors::illustrated-layout')

@section('title', __('Forbidden'))
@section('code', '403')
@section('message', 'Acceso no autorizado')
@section('image')
    <img src="{{asset('images/403.png')}}"  alt="403 imagen" style="max-height: 700px; max-width: 700px">
@endsection
