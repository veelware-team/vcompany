@extends('errors::illustrated-layout')

@section('title', __('Not Found'))
@section('code', '404')
@section('message','Disculpa, no hemos podido encontrar esa página.')
@section('image')
    <img src="{{asset('images/404.png')}}"  alt="404 imagen" style="max-height: 700px; max-width: 700px">
@endsection

