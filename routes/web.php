<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CustomerController ;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductBrandController;
use App\Http\Controllers\ProductGroupController;
use App\Http\Controllers\MeasurementUnitController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\ReportController;
Auth::routes(['verify' => false,'register' => false]);

Route::get('/',[\App\Http\Controllers\PublicController::class,'index'])->name('public.index');

Route::get('/loginRedirect', [\App\Http\Controllers\Auth\LoginController::class,'goToPage'])->name('login.redirect');
Route::post('/user/validate/email',[\App\Http\Controllers\Auth\ForgotPasswordController::class,'validateUserEmail'])->name('user.validate.email');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function (){
    //Dashboard
    Route::get('/dashboard', [DashboardController::class,'index'])->name('private.dashboard');
    //Dashboard submodulos
    Route::get('ventas-diarias',[DashboardController::class,'dailySales'])->name('dashboard.dailySales');
    Route::get('ventas-mensuales',[DashboardController::class,'monthlySales'])->name('dashboard.monthlySales');
    Route::get('clientes-totales',[DashboardController::class,'totalCustomers'])->name('dashboard.totalCustomers');
    Route::get('compras-mensuales',[DashboardController::class,'monthlyPurchases'])->name('dashboard.monthlyPurchases');
    Route::get('productos-poco-stock',[DashboardController::class,'lowStockProducts'])->name('dashboard.lowStockProducts');
    // Clientes
    Route::get('/clientes/list',[CustomerController::class,'list'])->name('customers.list');
    Route::get('/clientes/listDense',[CustomerController::class,'listDense'])->name('customers.list.dense');
    Route::resource('/clientes',CustomerController::class)
        ->parameters(['clientes '=>'customers'])->except(['create','edit','show'])->names('customers');
    // Clientes
    Route::get('/proveedores/list',[SupplierController::class,'list'])->name('suppliers.list');
    Route::resource('/proveedores', SupplierController::class)
        ->parameters(['proveedores'=>'suppliers'])->except(['create','edit','show'])->names('suppliers');
    // Categorias de producto
    Route::get('/categorias-producto/list',[ProductCategoryController::class,'list'])->name('productCategories.list');
    Route::resource('/categorias-producto',ProductCategoryController::class)
        ->parameters(['categorias-producto'=>'productCategories'])->except('create','edit','show')->names('productCategories');
    // Marcas de producto
    Route::get('marcas-producto/list',[ProductBrandController::class,'list'])->name('productBrands.list');
    Route::resource('/marcas-producto', productBrandController::class)
        ->parameters(['marcas-producto'=>'productBrands'])->except(['create','edit','show'])->names('productBrands');
    // Grupos de producto
    Route::get('grupos-producto/list',[ProductGroupController::class,'list'])->name('productGroups.list');
    Route::resource('grupos-producto',ProductGroupController::class)
        ->parameters(['grupos-producto'=>'productGroups'])->except(['create','edit','show'])->names('productGroups');
    // Unidades de medida
    Route::get('unidades-de-medida/list',[MeasurementUnitController::class,'list'])->name('measurementUnits.list');
    Route::resource('unidades-de-medida',MeasurementUnitController::class)
        ->parameters(['unidades-de-medida'=>'measurementUnits'])->except(['create','edit','show'])->names('measurementUnits');
    // Productos
    Route::post('productos/generateCode',[ProductController::class,'generateCode'])->name('products.generateCode');
    Route::get('productos/list',[ProductController::class,'list'])->name('products.list');
    Route::get('productos/listDense',[ProductController::class,'listDense'])->name('products.list.dense');
    Route::resource('productos',ProductController::class)
        ->parameters(['productos'=>'products'])->except(['create','edit','show'])->names('products');
    // Ventas
    Route::get('ventas',[SalesController::class,'index'])->name('sales.index');
    Route::post('ventas',[SalesController::class,'store'])->name('sales.store');
    Route::get('ventas/detalle/{sale}',[SalesController::class,'saleDetail'])->name('sales.detail');
    Route::post('ventas/cancelar',[SalesController::class,'cancelSale'])->name('sales.cancel');

    // Compras
    Route::get('compras',[PurchaseController::class,'index'])->name('purchases.index');
    Route::post('compras',[PurchaseController::class,'store'])->name('purchases.store');
    Route::get('compras/detalle/{purchase}',[PurchaseController::class,'purchaseDetail'])->name('purchases.detail');
    Route::post('compras/cancelar',[PurchaseController::class,'cancelPurchase'])->name('purchases.cancel');
/*
    Route::resource('ventas',SalesController::class)
        ->parameters(['ventas'=>'sales'])->except(['create','edit','show','destroy'])->names('sales');*/
    //Reporteria
    Route::get('reporteria/Inventario',[ReportController::class,'stock'])->name('reports.stock');
    Route::get('reporteria/Inventario-list',[ReportController::class,'listStock'])->name('reports.stock.list');
    Route::get('reporteria/historial-de-ventas',[ReportController::class,'salesHistory'])->name('reports.salesHistory');
    Route::get('reporteria/historial-de-compras',[ReportController::class,'purchasesHistory'])->name('reports.purchasesHistory');

});
